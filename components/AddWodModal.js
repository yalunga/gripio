import React, { Component } from 'react';
import fb from '../firebase.js'
import Router from 'next/router'
import uuidv4 from 'uuid/v4'

class AddWodModal extends Component {
  constructor() {
    super()
    this.state = {
      wodTitle: '',
      wodText: '',
      otherText: '',
      wodID: ''
    }
    this.handleChange = this.handleChange.bind(this)
    this.handleSave = this.handleSave.bind(this)
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSave() {
    const userId = fb.auth().currentUser.uid;
    const UUID = uuidv4();
    fb.firestore().collection('users').doc(userId).collection('wods').add({
      wodID: UUID,
      wodTitle: this.state.wodTitle,
      wodText: this.state.wodText,
      otherText: this.state.otherText,
      date: this.props.date
    }).then(function() {
      this.props.handler();
    }.bind(this));
  }

  render() {
    return(
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title has-text-info">Workout</p>
            <button className="delete" aria-label="close" onClick={this.props.handler}></button>
          </header>
          <section className="modal-card-body">
            <div className="field">
               <label className="label">Title</label>
               <div className="control">
                 <input
                   name="wodTitle"
                   className='input wod-title'
                   type="text"
                   placeholder="Fran"
                   value={this.state.wodTitle}
                   onChange={this.handleChange}

                 />
                 <div className="select">
                    <select>
                      <option>For Time</option>
                      <option>AMRAP</option>
                      <option>EMOM</option>
                      <option>Strength</option>
                    </select>
                  </div>
               </div>
               <p className="help is-danger"></p>
             </div>

             <div className="field">
               <label className="label">Workout</label>
               <div className="control">
                 <textarea
                   name="wodText"
                   className='textarea has-fixed-size'
                   type="text"
                   placeholder="21-15-9 Thrusters and Pullups"
                   value={this.state.wodText}
                   onChange={this.handleChange}

                />
               </div>
               <p className="help is-danger"></p>
             </div>

             <div className="field">
               <label className="label">Additional Details (Optional)</label>
               <div className="control">
                 <textarea
                   name="otherText"
                   className='textarea has-fixed-size'
                   type="text"
                   placeholder="You'll want to break into managable sets. Breathe at the top of each thruster."
                   value={this.state.otherText}
                   onChange={this.handleChange}

                />
               </div>
               <p className="help is-danger"></p>
             </div>

          </section>
          <footer className="modal-card-foot">
            <button className="button is-success is-outlined" onClick={this.handleSave}>
              Save
            </button>
            <button className="button is-danger is-outlined" onClick={this.props.handler}>
              Cancel
            </button>
          </footer>
        </div>
        <style jsx>{`
            .modal-card {
              width: 640px;
            }
            .wod-title {
              width: 70%;
              margin-right: 10%;
            }
          `}
        </style>
      </div>
    )
  }
}

export default AddWodModal;
