import React, { Component } from 'react';
import AddWodModal from './AddWodModal'
import BigCalendar from 'react-big-calendar'
import moment from 'moment'
import "react-big-calendar/lib/css/react-big-calendar.css";
import fb from '../firebase'
import Router from 'next/router'



const localizer = BigCalendar.momentLocalizer(moment)


class Calendar extends Component {
  constructor() {
    super()
    this.state = {
      events: [],
      modalActive: false,
    }
    this.onSelectWOD = this.onSelectWOD.bind(this)
    this.handleModal = this.handleModal.bind(this)
    this.modal = null;
  }

  componentDidMount() {
    const user = fb.auth().currentUser;
    if(user == null) {
      Router.push('/signup')
      return;
    } 
    const wodRef = fb.firestore().collection('users').doc(user.uid).collection('wods');
    let getWods = wodRef.get()
      .then(function(snapshot) {
        let events = [];
        snapshot.forEach(function(doc) {
          console.log(doc.id, '=>', doc.data());
          const event = {
            start: new moment(doc.data().date.seconds * 1000),
            end: new moment(doc.data().date.seconds * 1000),
            title: doc.data().wodTitle,
            allDay: true,
            resource: doc.data()
          }
          events.push(event);
        });
        this.setState({ events })
      }.bind(this)).catch(err => {
        console.log('Error getting documents', err);
      });
  }

  handleModal() {
    this.setState({modalActive: false})
  }

  onSelectWOD(e) {
    console.log(e)
    this.modal = <AddWodModal handler={this.handleModal} date={e.start} />
    this.setState({ modalActive: true })
  }

  render() {
    console.log(this.state.events)
    return(
      <div>
        {(this.state.modalActive) ? this.modal : ''}
        <section className="hero">
          <div className="hero-body">
            <div className="container">
              <BigCalendar
              localizer={localizer}
              events={this.state.events}
              startAccessor="start"
              endAccessor="end"
              views={{
                month: true,
                agenda: true
              }}
              selectable
              style={{ height: '90vh'}}
              onSelectEvent={this.onSelectWOD}
              onSelectSlot={this.onSelectWOD}
              />
            </div>
          </div>
        </section>
        <style jsx>{`
          .calenderStyle {
            height: 90vh;
            float: left;
          }
        `}</style>
      </div>
    )
  }
}

export default Calendar;
