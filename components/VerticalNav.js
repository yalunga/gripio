import React, { Component } from 'react';
import Head from 'next/head'
import '../styles/styles.sass'
import Link from 'next/link'
import fb from '../firebase.js'
import LoginModal from './LoginModal'
import Router from 'next/router'

class VerticalNav extends Component {

  constructor() {
    super();
  }

  render() {
    return(
      <div>
        <Head>
          <meta charset="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>grip.io</title>
          <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        </Head>
        <aside className="menu menu-style">
            <Link href="/">
              <a className="navbar-item">
                <img src={'../static/grip_logo.png'} width="125" height="60"/>
              </a>
            </Link>
            <p className="menu-label">
                General
            </p>
            <ul className="menu-list">
                <li><a>Dashboard</a></li>
                <li><a>Customers</a></li>
            </ul>
            <p className="menu-label">
                Administration
            </p>
            <ul className="menu-list">
                <li><a>Team Settings</a></li>
                <li>
                <a className="is-active">Manage Your Team</a>
                <ul>
                    <li><a>Members</a></li>
                    <li><a>Plugins</a></li>
                    <li><a>Add a member</a></li>
                </ul>
                </li>
                <li><a>Invitations</a></li>
                <li><a>Cloud Storage Environment Settings</a></li>
                <li><a>Authentication</a></li>
            </ul>
            <p className="menu-label">
                Transactions
            </p>
            <ul className="menu-list">
                <li><a>Payments</a></li>
                <li><a>Transfers</a></li>
                <li><a>Balance</a></li>
            </ul>
        </aside>
        {this.props.children}
        <style jsx>{`
            .menu-style {
                float:left;
                width: 15vw;
                margin-left: 5px;
            }
        `}</style>
    </div>
    )
  }
}


export default VerticalNav;
