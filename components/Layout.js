import React, { Component } from 'react';
import Head from 'next/head'
import '../styles/styles.sass'
import Link from 'next/link'
import fb from '../firebase.js'
import LoginModal from './LoginModal'
import Router from 'next/router'

class Layout extends Component {

  constructor() {
    super();
    this.state = {
      user: null,
      modalActive: false
    }
    this.signOut = this.signOut.bind(this);
    this.handleModal = this.handleModal.bind(this);
    this.loginClicked = this.loginClicked.bind(this);
  }

  componentDidMount() {
    const user = fb.auth().currentUser;
    if (user) {
      this.setState({ user });
    }
  }

  signOut() {
    fb.auth().signOut().then(function() {
      this.setState({ user: null })
      Router.push('/')
    }.bind(this)).catch(function(error) {
      // An error happened.
    });
  }

  loginClicked() {
    this.setState({ modalActive: true})
  }

  handleModal() {
    this.setState({modalActive: false})
  }

  render() {
    let modal;
    let userButtons;
    if(this.state.user) {
      userButtons = (
        <div className="buttons">
          <Link href="/console">
            <a className="button is-info is-outlined">
              Go To Console
            </a>
          </Link>
          <button className="button is-info is-outlined" onClick={this.signOut}>
            Sign Out
          </button>
        </div>
      )
    } else {
      userButtons = (
        <div className="buttons">
          <Link href="/signup">
            <a className="button is-info is-outlined">
              Sign up
            </a>
          </Link>
          <button className="button is-info is-outlined" onClick={this.loginClicked}>
            Log in
          </button>
        </div>
      )
    }

    if(this.state.modalActive) {
      modal = <LoginModal handler={this.handleModal}/>
    } else {
      modal = '';
    }

    return(
      <div>
        <Head>
          <meta charset="utf-8" />
          <meta name="viewport" content="width=device-width, initial-scale=1" />
          <title>grip.io</title>
          <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>
        </Head>
        {modal}
        <nav className="navbar is-white" role="navigation" aria-label="main navigation">
          <div className="navbar-brand">
            <Link href="/">
              <a className="navbar-item">
                <img src={'../static/grip_logo.png'} width="125" height="60"/>
              </a>
            </Link>

            <a role="button" className="navbar-burger burger" aria-label="menu" aria-expanded="false" data-target="navbarBasicExample">
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
              <span aria-hidden="true"></span>
            </a>
          </div>

          <div id="navbarBasicExample" className="navbar-menu">
            <div className="navbar-start">
              <a className="navbar-item">
                Home
              </a>

              <a className="navbar-item">
                About
              </a>

              <div className="navbar-item has-dropdown is-hoverable">
                <a className="navbar-link">
                  More
                </a>

                <div className="navbar-dropdown">
                  <a className="navbar-item">
                    About
                  </a>
                  <a className="navbar-item">
                    Jobs
                  </a>
                  <a className="navbar-item">
                    Contact
                  </a>
                  <hr className="navbar-divider" />
                  <a className="navbar-item">
                    Report an issue
                  </a>
                </div>
              </div>
            </div>

            <div className="navbar-end">
              <div className="navbar-item">
                {userButtons}
              </div>
            </div>
          </div>
        </nav>
        {this.props.children}
        <style jsx global>{`

          `}
        </style>
      </div>
    )
  }
}


export default Layout;
