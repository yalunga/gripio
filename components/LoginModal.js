import React, { Component } from 'react';
import fb from '../firebase.js'
import Router from 'next/router'

class LoginModal extends Component {
  constructor() {
    super()
    this.state = {
      email: '',
      password: '',
      emailHelpText: '',
      passwordHelpText: ''
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleLogin = this.handleLogin.bind(this);
    this.handleEmailFocus = this.handleEmailFocus.bind(this);
    this.handlePasswordFocus = this.handlePasswordFocus.bind(this);
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleLogin() {
    fb.auth().signInWithEmailAndPassword(this.state.email, this.state.password)
    .catch(function(error) {
      const errorCode = error.code;
      const errorMessage = error.message;
      if(errorCode == 'auth/invalid-email' || errorCode == 'auth/user-not-found'){
        this.setState({ emailHelpText: 'Email not found.' })
      }
      if(errorCode == 'auth/wrong-password') {
        this.setState({ passwordHelpText: 'The password is invalid.' })
      }
    }.bind(this)).then(() => {
      const user = fb.auth().currentUser;
      if (user) {
        this.props.handler();
        Router.push('/console')
      }
    })
  }

  handleEmailFocus() {
    this.setState({ emailHelpText: ''})
  }

  handlePasswordFocus() {
    this.setState({ passwordHelpText: ''})
  }

  render() {
    let emailStyle = ["input", "is-rounded"]
    let passwordStyle = ["input", "is-rounded"]

    if(this.state.emailHelpText != ''){
      emailStyle.push('is-danger')
    } else {
      emailStyle = ["input", "is-rounded"]
    }
    if(this.state.passwordHelpText != '') {
      passwordStyle.push('is-danger')
    } else {
      passwordStyle = ["input", "is-rounded"]
    }

    return(
      <div className="modal is-active">
        <div className="modal-background"></div>
        <div className="modal-card">
          <header className="modal-card-head">
            <p className="modal-card-title has-text-info">Login</p>
            <button className="delete" aria-label="close" onClick={this.props.handler}></button>
          </header>
          <section className="modal-card-body">
            <div className="field">
               <label className="label">Email</label>
               <div className="control has-icons-left">
                 <input
                   name="email"
                   className={emailStyle.join(' ')}
                   type="email"
                   placeholder="e.g. alexsmith@gmail.com"
                   value={this.state.email}
                   onChange={this.handleChange}
                   onFocus={this.handleEmailFocus}
                 />
                 <span className="icon is-small is-left">
                   <i className="fas fa-envelope"></i>
                 </span>
               </div>
               <p className="help is-danger">{this.state.emailHelpText}</p>
             </div>

             <div className="field">
               <label className="label">Password</label>
               <div className="control has-icons-left">
                 <input
                   name="password"
                   className={passwordStyle.join(' ')}
                   type="password"
                   value={this.state.password}
                   onChange={this.handleChange}
                   onFocus={this.handlePasswordFocus}
                />
                 <span className="icon is-small is-left">
                   <i className="fas fa-lock"></i>
                 </span>
               </div>
               <p className="help is-danger">{this.state.passwordHelpText}</p>
             </div>
          </section>
          <footer className="modal-card-foot">
            <button className="button is-info is-outlined" onClick={this.handleLogin}>
              Login
            </button>
            <button className="button is-danger is-outlined" onClick={this.props.handler}>
              Cancel
            </button>
          </footer>
        </div>
        <style jsx>{`
            .modal-card {
              width: 500px;
            }
          `}
        </style>
      </div>
    )
  }
}

export default LoginModal;
