import React, { Component } from 'react';
import VerticalNav from '../components/VerticalNav';
import Calendar from '../components/Calendar';
import fb from '../firebase'
import Router from 'next/router'

class Console extends Component {
  constructor() {
    super()
    this.state = {
    
    }
    this.onSelectWOD = this.onSelectWOD.bind(this)
    this.handleModal = this.handleModal.bind(this)
    this.modal = null;
  }

  componentDidMount() {
    const user = fb.auth().currentUser;
    if(user == null) {
      Router.push('/signup')
      return;
    } 
  }

  handleModal() {
    this.setState({modalActive: false})
  }

  onSelectWOD(e) {
    console.log(e)
    this.modal = <AddWodModal handler={this.handleModal} date={e.start} />
    this.setState({ modalActive: true })
  }

  render() {
    console.log(this.state.events)
    return(
      <VerticalNav>
       <Calendar />
        <style jsx>{`
          .calenderStyle {
            height: 90vh;
            float: left;
          }
        `}</style>
      </VerticalNav>
    )
  }
}

export default Console;
