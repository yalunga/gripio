import React, {Component} from 'react';
import Router from 'next/router';
import Layout from '../components/Layout';
import fb from '../firebase.js';


class Signup extends Component {
  constructor() {
    super();
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      validEmail: true,
      validPassword: true,
      validConfirm: true,
      emailHelpText: '',
      passwordHelpText: '',
      confirmHelpText: '',
    }
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
    this.handleEmailFocus = this.handleEmailFocus.bind(this);
    this.handlePasswordFocus = this.handlePasswordFocus.bind(this);
    this.handleConfirmFocus = this.handleConfirmFocus.bind(this);
  }

  handleEmailFocus() {
    this.setState({ validEmail: true })
    this.setState({ emailHelpText: ''})
  }

  handlePasswordFocus() {
    this.setState({ validPassword: true })
    this.setState({ passwordHelpText: ''})
  }

  handleConfirmFocus() {
    this.setState({ validConfirm: true })
    this.setState({ confirmHelpText: '' })
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value });
  }

  handleSubmit() {
    let validInputs = true;
    if(this.state.email == ''){
      validInputs = false;
      this.setState({ validEmail: false })
      this.setState({ emailHelpText: 'Email required.'})
    }
    if(this.state.password == ''){
      validInputs = false;
      this.setState({ validPassword: false })
      this.setState({ passwordHelpText: 'Password required.'})
    }
    if(this.state.password != this.state.confirmPassword) {
      validInputs = false;
      this.setState({ validConfirm: false })
      this.setState({ confirmHelpText: 'Passwords must match.' })
    }
    if(validInputs) {
      fb.auth().createUserWithEmailAndPassword(this.state.email, this.state.password).catch(function(error) {
        const errorCode = error.code;
        const errorMessage = error.message;
        console.log(errorMessage);
        if(errorCode == 'auth/email-already-in-use') {
          this.setState({ validEmail: false })
          this.setState({ emailHelpText: errorMessage})
        } else if(errorCode == 'auth/invalid-email') {
          this.setState({ validEmail: false })
          this.setState({ emailHelpText: errorMessage})
        } else if(errorCode == 'auth/weak-password') {
          this.setState({ validPassword: false })
          this.setState({ passwordHelpText: errorMessage})
        }
      }.bind(this)).then(() => {
        const user = fb.auth().currentUser;
        if (user) {
          Router.push('/')
        }
      });
    }
  }

  render() {
    let emailStyle = ["input", "is-rounded"]
    let passwordStyle = ["input", "is-rounded"]
    let confirmStyle = ["input", "is-rounded"]

    if(!this.state.validEmail){
      emailStyle.push('is-danger')
    } else {
      emailStyle = ["input", "is-rounded"]
    }
    if(!this.state.validPassword) {
      passwordStyle.push('is-danger')
    } else {
      passwordStyle = ["input", "is-rounded"]
    }
    if(!this.state.validConfirm){
      confirmStyle.push('is-danger')
    } else {
      confirmStyle = ["input", "is-rounded"]
    }
    return (
      <Layout>
        <section className="hero is-fullheight-with-navbar is-info">
         <div className="hero-body">
          <div className="container">
            <div className="columns is-vcentered">
             <div className="column has-text-centered">
                <h1 className="title">
                  Sign up with us today!
                </h1>
                <h2 className="subtitle">
                  grip.io allows you to effortlessly communicate with your members.
                </h2>
             </div>
             <div className="column has-text-centered">

             <div className="field">
                <label className="label has-text-white">Email</label>
                <div className="control has-icons-left">
                  <input
                    name="email"
                    className={emailStyle.join(' ')}
                    value={this.state.email}
                    onChange={this.handleChange}
                    type="email"
                    placeholder="e.g. alexsmith@gmail.com"
                    onFocus={this.handleEmailFocus} />
                  <span className="icon is-small is-left">
                    <i className="fas fa-envelope"></i>
                  </span>
                </div>
                <p className="help is-danger">{this.state.emailHelpText}</p>
              </div>

              <div className="field">
                <label className="label has-text-white">Password</label>
                <div className="control has-icons-left">
                  <input
                    name="password"
                    className={passwordStyle.join(' ')}
                    type="password"
                    value={this.state.password}
                    onChange={this.handleChange}
                    onFocus={this.handlePasswordFocus}/>
                  <span className="icon is-small is-left">
                    <i className="fas fa-lock"></i>
                  </span>
                </div>
                <p className="help is-danger">{this.state.passwordHelpText}</p>
              </div>

              <div className="field">
                <label className="label has-text-white">Confirm Password</label>
                <div className="control has-icons-left">
                  <input
                    name="confirmPassword"
                    className={confirmStyle.join(' ')}
                    type="password"
                    value={this.state.confirmPassword}
                    onChange={this.handleChange}
                    onFocus={this.handleConfirmFocus}/>
                  <span className="icon is-small is-left">
                    <i className="fas fa-lock"></i>
                  </span>
                </div>
                <p className="help is-danger">{this.state.confirmHelpText}</p>
              </div>

              <button onClick={this.handleSubmit} className="button is-primary is-inverted is-rounded is-large">Sign Up</button>

             </div>
            </div>
          </div>
         </div>
        </section>
      </Layout>
    )
  }
}

export default Signup;
