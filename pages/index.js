import React, { Component } from 'react';
import Layout from '../components/Layout';

class Index extends Component {

  render() {
    return(
      <Layout>
        <section className="hero is-fullheight-with-navbar">
         <div className="hero-body">
          <div className="container">
           <div className="columns">
            <div className="column has-text-centered">
               <h1 className="title has-text-info">
                 Bring your gym together.
               </h1>
               <h2 className="subtitle has-text-info">
                 grip.io allows you to effortlessly communicate with your members.
               </h2>
            </div>
            <div className="column has-text-centered">
              <h1 className="title">
                Photo goes here.
              </h1>
              <h2 className="subtitle">
                Hero subtitle
              </h2>
            </div>
           </div>
          </div>
         </div>
        </section>
      </Layout>
    )
  }
}

export default Index;
