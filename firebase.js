import firebase from 'firebase';

const config = {
    apiKey: "AIzaSyBnPR-8wZTA-UhJnqlotJZbZh96L7r1Sok",
    authDomain: "gripio-374d7.firebaseapp.com",
    databaseURL: "https://gripio-374d7.firebaseio.com",
    projectId: "gripio-374d7",
    storageBucket: "gripio-374d7.appspot.com",
    messagingSenderId: "940281243850"
};

export default !firebase.apps.length ? firebase.initializeApp(config) : firebase.app();
