webpackHotUpdate("static/development/pages/console.js",{

/***/ "./components/Calendar.js":
/*!********************************!*\
  !*** ./components/Calendar.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _AddWodModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddWodModal */ "./components/AddWodModal.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-big-calendar */ "./node_modules/react-big-calendar/lib/index.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-big-calendar/lib/css/react-big-calendar.css */ "./node_modules/react-big-calendar/lib/css/react-big-calendar.css");
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../firebase */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/andyyalung/gripio/components/Calendar.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var localizer = react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a.momentLocalizer(moment__WEBPACK_IMPORTED_MODULE_4___default.a);

var Calendar =
/*#__PURE__*/
function (_Component) {
  _inherits(Calendar, _Component);

  function Calendar() {
    var _this;

    _classCallCheck(this, Calendar);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Calendar).call(this));
    _this.state = {
      events: [{
        start: new Date(),
        end: new Date(),
        title: "Fran",
        allDay: true,
        resource: {
          id: 1
        }
      }],
      modalActive: false
    };
    _this.onSelectWOD = _this.onSelectWOD.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.modal = null;
    return _this;
  }

  _createClass(Calendar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var user = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().currentUser;

      if (user == null) {
        next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push('/signup');
        return;
      }

      var wodRef = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].firestore().collection('users').doc(user.uid).collection('wods');
      var getWods = wodRef.get().then(function (snapshot) {
        snapshot.forEach(function (doc) {
          console.log(doc.id, '=>', doc.data());
          var event = {
            start: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            end: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            title: doc.data().wodTitle,
            allDay: true,
            resource: doc.data()
          };

          _this2.state.events.push(event);
        });
      }).catch(function (err) {
        console.log('Error getting documents', err);
      });
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "onSelectWOD",
    value: function onSelectWOD(e) {
      console.log(e);
      this.modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddWodModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handler: this.handleModal,
        date: e.start,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 65
        },
        __self: this
      });
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.events);
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, this.state.modalActive ? this.modal : '', react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-2494239222" + " " + "hero",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "hero-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a, {
        localizer: localizer,
        events: this.state.events,
        startAccessor: "start",
        endAccessor: "end",
        views: {
          month: true,
          agenda: true
        },
        selectable: true,
        style: {
          height: '90vh'
        },
        onSelectEvent: this.onSelectWOD,
        onSelectSlot: this.onSelectWOD,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      })))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2494239222",
        css: ".calenderStyle.jsx-2494239222{height:90vh;float:left;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQTZGb0IsQUFHeUIsWUFDRCxXQUNiIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBBZGRXb2RNb2RhbCBmcm9tICcuL0FkZFdvZE1vZGFsJ1xuaW1wb3J0IEJpZ0NhbGVuZGFyIGZyb20gJ3JlYWN0LWJpZy1jYWxlbmRhcidcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50J1xuaW1wb3J0IFwicmVhY3QtYmlnLWNhbGVuZGFyL2xpYi9jc3MvcmVhY3QtYmlnLWNhbGVuZGFyLmNzc1wiO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcblxuXG5cbmNvbnN0IGxvY2FsaXplciA9IEJpZ0NhbGVuZGFyLm1vbWVudExvY2FsaXplcihtb21lbnQpXG5cblxuY2xhc3MgQ2FsZW5kYXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGV2ZW50czogW1xuICAgICAgICB7XG4gICAgICAgICAgc3RhcnQ6IG5ldyBEYXRlKCksXG4gICAgICAgICAgZW5kOiBuZXcgRGF0ZSgpLFxuICAgICAgICAgIHRpdGxlOiBcIkZyYW5cIixcbiAgICAgICAgICBhbGxEYXk6IHRydWUsXG4gICAgICAgICAgcmVzb3VyY2U6IHtpZDogMX1cbiAgICAgICAgfVxuICAgICAgXSxcbiAgICAgIG1vZGFsQWN0aXZlOiBmYWxzZSxcbiAgICB9XG4gICAgdGhpcy5vblNlbGVjdFdPRCA9IHRoaXMub25TZWxlY3RXT0QuYmluZCh0aGlzKVxuICAgIHRoaXMuaGFuZGxlTW9kYWwgPSB0aGlzLmhhbmRsZU1vZGFsLmJpbmQodGhpcylcbiAgICB0aGlzLm1vZGFsID0gbnVsbDtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHVzZXIgPSBmYi5hdXRoKCkuY3VycmVudFVzZXI7XG4gICAgaWYodXNlciA9PSBudWxsKSB7XG4gICAgICBSb3V0ZXIucHVzaCgnL3NpZ251cCcpXG4gICAgICByZXR1cm47XG4gICAgfSBcbiAgICBjb25zdCB3b2RSZWYgPSBmYi5maXJlc3RvcmUoKS5jb2xsZWN0aW9uKCd1c2VycycpLmRvYyh1c2VyLnVpZCkuY29sbGVjdGlvbignd29kcycpO1xuICAgIGxldCBnZXRXb2RzID0gd29kUmVmLmdldCgpXG4gICAgICAudGhlbihzbmFwc2hvdCA9PiB7XG4gICAgICAgIHNuYXBzaG90LmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhkb2MuaWQsICc9PicsIGRvYy5kYXRhKCkpO1xuICAgICAgICAgIGNvbnN0IGV2ZW50ID0ge1xuICAgICAgICAgICAgc3RhcnQ6IG5ldyBtb21lbnQoZG9jLmRhdGEoKS5kYXRlLnNlY29uZHMgKiAxMDAwKSxcbiAgICAgICAgICAgIGVuZDogbmV3IG1vbWVudChkb2MuZGF0YSgpLmRhdGUuc2Vjb25kcyAqIDEwMDApLFxuICAgICAgICAgICAgdGl0bGU6IGRvYy5kYXRhKCkud29kVGl0bGUsXG4gICAgICAgICAgICBhbGxEYXk6IHRydWUsXG4gICAgICAgICAgICByZXNvdXJjZTogZG9jLmRhdGEoKVxuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnN0YXRlLmV2ZW50cy5wdXNoKGV2ZW50KTtcbiAgICAgICAgfSk7XG4gICAgICB9KS5jYXRjaChlcnIgPT4ge1xuICAgICAgICBjb25zb2xlLmxvZygnRXJyb3IgZ2V0dGluZyBkb2N1bWVudHMnLCBlcnIpO1xuICAgICAgfSk7XG4gIH1cblxuICBoYW5kbGVNb2RhbCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKHttb2RhbEFjdGl2ZTogZmFsc2V9KVxuICB9XG5cbiAgb25TZWxlY3RXT0QoZSkge1xuICAgIGNvbnNvbGUubG9nKGUpXG4gICAgdGhpcy5tb2RhbCA9IDxBZGRXb2RNb2RhbCBoYW5kbGVyPXt0aGlzLmhhbmRsZU1vZGFsfSBkYXRlPXtlLnN0YXJ0fSAvPlxuICAgIHRoaXMuc2V0U3RhdGUoeyBtb2RhbEFjdGl2ZTogdHJ1ZSB9KVxuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIGNvbnNvbGUubG9nKHRoaXMuc3RhdGUuZXZlbnRzKVxuICAgIHJldHVybihcbiAgICAgIDxkaXY+XG4gICAgICAgIHsodGhpcy5zdGF0ZS5tb2RhbEFjdGl2ZSkgPyB0aGlzLm1vZGFsIDogJyd9XG4gICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT1cImhlcm9cIj5cbiAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImhlcm8tYm9keVwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250YWluZXJcIj5cbiAgICAgICAgICAgICAgPEJpZ0NhbGVuZGFyXG4gICAgICAgICAgICAgIGxvY2FsaXplcj17bG9jYWxpemVyfVxuICAgICAgICAgICAgICBldmVudHM9e3RoaXMuc3RhdGUuZXZlbnRzfVxuICAgICAgICAgICAgICBzdGFydEFjY2Vzc29yPVwic3RhcnRcIlxuICAgICAgICAgICAgICBlbmRBY2Nlc3Nvcj1cImVuZFwiXG4gICAgICAgICAgICAgIHZpZXdzPXt7XG4gICAgICAgICAgICAgICAgbW9udGg6IHRydWUsXG4gICAgICAgICAgICAgICAgYWdlbmRhOiB0cnVlXG4gICAgICAgICAgICAgIH19XG4gICAgICAgICAgICAgIHNlbGVjdGFibGVcbiAgICAgICAgICAgICAgc3R5bGU9e3sgaGVpZ2h0OiAnOTB2aCd9fVxuICAgICAgICAgICAgICBvblNlbGVjdEV2ZW50PXt0aGlzLm9uU2VsZWN0V09EfVxuICAgICAgICAgICAgICBvblNlbGVjdFNsb3Q9e3RoaXMub25TZWxlY3RXT0R9XG4gICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgPC9zZWN0aW9uPlxuICAgICAgICA8c3R5bGUganN4PntgXG4gICAgICAgICAgLmNhbGVuZGVyU3R5bGUge1xuICAgICAgICAgICAgaGVpZ2h0OiA5MHZoO1xuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgICAgfVxuICAgICAgICBgfTwvc3R5bGU+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQ2FsZW5kYXI7XG4iXX0= */\n/*@ sourceURL=/Users/andyyalung/gripio/components/Calendar.js */",
        __self: this
      }));
    }
  }]);

  return Calendar;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Calendar);

/***/ })

})
//# sourceMappingURL=console.js.3c7c9be2412b83ef67dc.hot-update.js.map