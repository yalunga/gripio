webpackHotUpdate("static/development/pages/console.js",{

/***/ "./components/Calendar.js":
/*!********************************!*\
  !*** ./components/Calendar.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _AddWodModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddWodModal */ "./components/AddWodModal.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-big-calendar */ "./node_modules/react-big-calendar/lib/index.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-big-calendar/lib/css/react-big-calendar.css */ "./node_modules/react-big-calendar/lib/css/react-big-calendar.css");
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../firebase */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/andyyalung/gripio/components/Calendar.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var localizer = react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a.momentLocalizer(moment__WEBPACK_IMPORTED_MODULE_4___default.a);

var Calendar =
/*#__PURE__*/
function (_Component) {
  _inherits(Calendar, _Component);

  function Calendar() {
    var _this;

    _classCallCheck(this, Calendar);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Calendar).call(this));
    _this.state = {
      events: [{
        start: new Date(),
        end: new Date(),
        title: "Fran",
        allDay: true,
        resource: {
          id: 1
        }
      }, {
        start: new Date(),
        end: new Date(),
        title: "Fran2",
        allDay: true,
        resource: {
          id: 1
        }
      }],
      modalActive: false
    };
    _this.onSelectWOD = _this.onSelectWOD.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.modal = null;
    return _this;
  }

  _createClass(Calendar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var user = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().currentUser;

      if (user == null) {
        next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push('/signup');
        return;
      }

      var wodRef = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].firestore().collection('users').doc(user.uid).collection('wods');
      var getWods = wodRef.get().then(function (snapshot) {
        var _this2 = this;

        snapshot.forEach(function (doc) {
          console.log(doc.id, '=>', doc.data());
          var event = {
            start: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            end: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            title: doc.data().wodTitle,
            allDay: true,
            resource: doc.data()
          };

          _this2.state.events.push(event);
        });
      }.bind(this)).catch(function (err) {
        console.log('Error getting documents', err);
      });
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "onSelectWOD",
    value: function onSelectWOD(e) {
      console.log(e);
      this.modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddWodModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handler: this.handleModal,
        date: e.start,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      });
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.events);
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }, this.state.modalActive ? this.modal : '', react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-2494239222" + " " + "hero",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 81
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "hero-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 82
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 83
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a, {
        localizer: localizer,
        events: this.state.events,
        startAccessor: "start",
        endAccessor: "end",
        views: {
          month: true,
          agenda: true
        },
        selectable: true,
        style: {
          height: '90vh'
        },
        onSelectEvent: this.onSelectWOD,
        onSelectSlot: this.onSelectWOD,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      })))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2494239222",
        css: ".calenderStyle.jsx-2494239222{height:90vh;float:left;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQW9Hb0IsQUFHeUIsWUFDRCxXQUNiIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBBZGRXb2RNb2RhbCBmcm9tICcuL0FkZFdvZE1vZGFsJ1xuaW1wb3J0IEJpZ0NhbGVuZGFyIGZyb20gJ3JlYWN0LWJpZy1jYWxlbmRhcidcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50J1xuaW1wb3J0IFwicmVhY3QtYmlnLWNhbGVuZGFyL2xpYi9jc3MvcmVhY3QtYmlnLWNhbGVuZGFyLmNzc1wiO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcblxuXG5cbmNvbnN0IGxvY2FsaXplciA9IEJpZ0NhbGVuZGFyLm1vbWVudExvY2FsaXplcihtb21lbnQpXG5cblxuY2xhc3MgQ2FsZW5kYXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGV2ZW50czogW1xuICAgICAgICB7XG4gICAgICAgICAgc3RhcnQ6IG5ldyBEYXRlKCksXG4gICAgICAgICAgZW5kOiBuZXcgRGF0ZSgpLFxuICAgICAgICAgIHRpdGxlOiBcIkZyYW5cIixcbiAgICAgICAgICBhbGxEYXk6IHRydWUsXG4gICAgICAgICAgcmVzb3VyY2U6IHtpZDogMX1cbiAgICAgICAgfSxcbiAgICAgICAge1xuICAgICAgICAgICAgc3RhcnQ6IG5ldyBEYXRlKCksXG4gICAgICAgICAgICBlbmQ6IG5ldyBEYXRlKCksXG4gICAgICAgICAgICB0aXRsZTogXCJGcmFuMlwiLFxuICAgICAgICAgICAgYWxsRGF5OiB0cnVlLFxuICAgICAgICAgICAgcmVzb3VyY2U6IHtpZDogMX1cbiAgICAgICAgICB9XG4gICAgICBdLFxuICAgICAgbW9kYWxBY3RpdmU6IGZhbHNlLFxuICAgIH1cbiAgICB0aGlzLm9uU2VsZWN0V09EID0gdGhpcy5vblNlbGVjdFdPRC5iaW5kKHRoaXMpXG4gICAgdGhpcy5oYW5kbGVNb2RhbCA9IHRoaXMuaGFuZGxlTW9kYWwuYmluZCh0aGlzKVxuICAgIHRoaXMubW9kYWwgPSBudWxsO1xuICB9XG5cbiAgY29tcG9uZW50RGlkTW91bnQoKSB7XG4gICAgY29uc3QgdXNlciA9IGZiLmF1dGgoKS5jdXJyZW50VXNlcjtcbiAgICBpZih1c2VyID09IG51bGwpIHtcbiAgICAgIFJvdXRlci5wdXNoKCcvc2lnbnVwJylcbiAgICAgIHJldHVybjtcbiAgICB9IFxuICAgIGNvbnN0IHdvZFJlZiA9IGZiLmZpcmVzdG9yZSgpLmNvbGxlY3Rpb24oJ3VzZXJzJykuZG9jKHVzZXIudWlkKS5jb2xsZWN0aW9uKCd3b2RzJyk7XG4gICAgbGV0IGdldFdvZHMgPSB3b2RSZWYuZ2V0KClcbiAgICAgIC50aGVuKGZ1bmN0aW9uKHNuYXBzaG90KSB7XG4gICAgICAgIHNuYXBzaG90LmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhkb2MuaWQsICc9PicsIGRvYy5kYXRhKCkpO1xuICAgICAgICAgIGNvbnN0IGV2ZW50ID0ge1xuICAgICAgICAgICAgc3RhcnQ6IG5ldyBtb21lbnQoZG9jLmRhdGEoKS5kYXRlLnNlY29uZHMgKiAxMDAwKSxcbiAgICAgICAgICAgIGVuZDogbmV3IG1vbWVudChkb2MuZGF0YSgpLmRhdGUuc2Vjb25kcyAqIDEwMDApLFxuICAgICAgICAgICAgdGl0bGU6IGRvYy5kYXRhKCkud29kVGl0bGUsXG4gICAgICAgICAgICBhbGxEYXk6IHRydWUsXG4gICAgICAgICAgICByZXNvdXJjZTogZG9jLmRhdGEoKVxuICAgICAgICAgIH1cbiAgICAgICAgICB0aGlzLnN0YXRlLmV2ZW50cy5wdXNoKGV2ZW50KTtcbiAgICAgICAgfSk7XG4gICAgICB9LmJpbmQodGhpcykpLmNhdGNoKGVyciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdFcnJvciBnZXR0aW5nIGRvY3VtZW50cycsIGVycik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGhhbmRsZU1vZGFsKCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe21vZGFsQWN0aXZlOiBmYWxzZX0pXG4gIH1cblxuICBvblNlbGVjdFdPRChlKSB7XG4gICAgY29uc29sZS5sb2coZSlcbiAgICB0aGlzLm1vZGFsID0gPEFkZFdvZE1vZGFsIGhhbmRsZXI9e3RoaXMuaGFuZGxlTW9kYWx9IGRhdGU9e2Uuc3RhcnR9IC8+XG4gICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGFsQWN0aXZlOiB0cnVlIH0pXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5ldmVudHMpXG4gICAgcmV0dXJuKFxuICAgICAgPGRpdj5cbiAgICAgICAgeyh0aGlzLnN0YXRlLm1vZGFsQWN0aXZlKSA/IHRoaXMubW9kYWwgOiAnJ31cbiAgICAgICAgPHNlY3Rpb24gY2xhc3NOYW1lPVwiaGVyb1wiPlxuICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiaGVyby1ib2R5XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRhaW5lclwiPlxuICAgICAgICAgICAgICA8QmlnQ2FsZW5kYXJcbiAgICAgICAgICAgICAgbG9jYWxpemVyPXtsb2NhbGl6ZXJ9XG4gICAgICAgICAgICAgIGV2ZW50cz17dGhpcy5zdGF0ZS5ldmVudHN9XG4gICAgICAgICAgICAgIHN0YXJ0QWNjZXNzb3I9XCJzdGFydFwiXG4gICAgICAgICAgICAgIGVuZEFjY2Vzc29yPVwiZW5kXCJcbiAgICAgICAgICAgICAgdmlld3M9e3tcbiAgICAgICAgICAgICAgICBtb250aDogdHJ1ZSxcbiAgICAgICAgICAgICAgICBhZ2VuZGE6IHRydWVcbiAgICAgICAgICAgICAgfX1cbiAgICAgICAgICAgICAgc2VsZWN0YWJsZVxuICAgICAgICAgICAgICBzdHlsZT17eyBoZWlnaHQ6ICc5MHZoJ319XG4gICAgICAgICAgICAgIG9uU2VsZWN0RXZlbnQ9e3RoaXMub25TZWxlY3RXT0R9XG4gICAgICAgICAgICAgIG9uU2VsZWN0U2xvdD17dGhpcy5vblNlbGVjdFdPRH1cbiAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L3NlY3Rpb24+XG4gICAgICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgICAgICAuY2FsZW5kZXJTdHlsZSB7XG4gICAgICAgICAgICBoZWlnaHQ6IDkwdmg7XG4gICAgICAgICAgICBmbG9hdDogbGVmdDtcbiAgICAgICAgICB9XG4gICAgICAgIGB9PC9zdHlsZT5cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBDYWxlbmRhcjtcbiJdfQ== */\n/*@ sourceURL=/Users/andyyalung/gripio/components/Calendar.js */",
        __self: this
      }));
    }
  }]);

  return Calendar;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Calendar);

/***/ })

})
//# sourceMappingURL=console.js.a212e77f552bd86bf6ab.hot-update.js.map