webpackHotUpdate("static/development/pages/console.js",{

/***/ "./pages/console.js":
/*!**************************!*\
  !*** ./pages/console.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* WEBPACK VAR INJECTION */(function(module) {/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_VerticalNav__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/VerticalNav */ "./components/VerticalNav.js");
/* harmony import */ var _components_Calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Calendar */ "./components/Calendar.js");
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../firebase */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/andyyalung/gripio/pages/console.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var Console =
/*#__PURE__*/
function (_Component) {
  _inherits(Console, _Component);

  function Console() {
    var _this;

    _classCallCheck(this, Console);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Console).call(this));
    _this.state = {};
    _this.onSelectWOD = _this.onSelectWOD.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.modal = null;
    return _this;
  }

  _createClass(Console, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var _this2 = this;

      var user = _firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().currentUser;

      if (user == null) {
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push('/signup');
        return;
      }

      var wodRef = _firebase__WEBPACK_IMPORTED_MODULE_4__["default"].firestore().collection('users').doc(user.uid).collection('wods');
      var getWods = wodRef.get().then(function (snapshot) {
        snapshot.forEach(function (doc) {
          console.log(doc.id, '=>', doc.data());
          var event = {
            start: new Date(doc.data().date.seconds * 1000),
            end: new Date(doc.data().date.seconds * 1000),
            title: doc.data().wodTitle,
            allDay: true,
            resource: doc.data()
          };

          _this2.state.events.push(event);
        });
      }).catch(function (err) {
        console.log('Error getting documents', err);
      });
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "onSelectWOD",
    value: function onSelectWOD(e) {
      console.log(e);
      this.modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(AddWodModal, {
        handler: this.handleModal,
        date: e.start,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      });
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.events);
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_VerticalNav__WEBPACK_IMPORTED_MODULE_2__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 56
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_Calendar__WEBPACK_IMPORTED_MODULE_3__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2494239222",
        css: ".calenderStyle.jsx-2494239222{height:90vh;float:left;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9wYWdlcy9jb25zb2xlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXlEb0IsQUFHeUIsWUFDRCxXQUNiIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9wYWdlcy9jb25zb2xlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBWZXJ0aWNhbE5hdiBmcm9tICcuLi9jb21wb25lbnRzL1ZlcnRpY2FsTmF2JztcbmltcG9ydCBDYWxlbmRhciBmcm9tICcuLi9jb21wb25lbnRzL0NhbGVuZGFyJztcbmltcG9ydCBmYiBmcm9tICcuLi9maXJlYmFzZSdcbmltcG9ydCBSb3V0ZXIgZnJvbSAnbmV4dC9yb3V0ZXInXG5cbmNsYXNzIENvbnNvbGUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICBcbiAgICB9XG4gICAgdGhpcy5vblNlbGVjdFdPRCA9IHRoaXMub25TZWxlY3RXT0QuYmluZCh0aGlzKVxuICAgIHRoaXMuaGFuZGxlTW9kYWwgPSB0aGlzLmhhbmRsZU1vZGFsLmJpbmQodGhpcylcbiAgICB0aGlzLm1vZGFsID0gbnVsbDtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHVzZXIgPSBmYi5hdXRoKCkuY3VycmVudFVzZXI7XG4gICAgaWYodXNlciA9PSBudWxsKSB7XG4gICAgICBSb3V0ZXIucHVzaCgnL3NpZ251cCcpXG4gICAgICByZXR1cm47XG4gICAgfSBcbiAgICBjb25zdCB3b2RSZWYgPSBmYi5maXJlc3RvcmUoKS5jb2xsZWN0aW9uKCd1c2VycycpLmRvYyh1c2VyLnVpZCkuY29sbGVjdGlvbignd29kcycpO1xuICAgIGxldCBnZXRXb2RzID0gd29kUmVmLmdldCgpXG4gICAgICAudGhlbihzbmFwc2hvdCA9PiB7XG4gICAgICAgIHNuYXBzaG90LmZvckVhY2goZG9jID0+IHtcbiAgICAgICAgICBjb25zb2xlLmxvZyhkb2MuaWQsICc9PicsIGRvYy5kYXRhKCkpO1xuICAgICAgICAgIGNvbnN0IGV2ZW50ID0ge1xuICAgICAgICAgICAgc3RhcnQ6IG5ldyBEYXRlKGRvYy5kYXRhKCkuZGF0ZS5zZWNvbmRzICogMTAwMCksXG4gICAgICAgICAgICBlbmQ6IG5ldyBEYXRlKGRvYy5kYXRhKCkuZGF0ZS5zZWNvbmRzICogMTAwMCksXG4gICAgICAgICAgICB0aXRsZTogZG9jLmRhdGEoKS53b2RUaXRsZSxcbiAgICAgICAgICAgIGFsbERheTogdHJ1ZSxcbiAgICAgICAgICAgIHJlc291cmNlOiBkb2MuZGF0YSgpXG4gICAgICAgICAgfVxuICAgICAgICAgIHRoaXMuc3RhdGUuZXZlbnRzLnB1c2goZXZlbnQpO1xuICAgICAgICB9KTtcbiAgICAgIH0pLmNhdGNoKGVyciA9PiB7XG4gICAgICAgIGNvbnNvbGUubG9nKCdFcnJvciBnZXR0aW5nIGRvY3VtZW50cycsIGVycik7XG4gICAgICB9KTtcbiAgfVxuXG4gIGhhbmRsZU1vZGFsKCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe21vZGFsQWN0aXZlOiBmYWxzZX0pXG4gIH1cblxuICBvblNlbGVjdFdPRChlKSB7XG4gICAgY29uc29sZS5sb2coZSlcbiAgICB0aGlzLm1vZGFsID0gPEFkZFdvZE1vZGFsIGhhbmRsZXI9e3RoaXMuaGFuZGxlTW9kYWx9IGRhdGU9e2Uuc3RhcnR9IC8+XG4gICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGFsQWN0aXZlOiB0cnVlIH0pXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5ldmVudHMpXG4gICAgcmV0dXJuKFxuICAgICAgPFZlcnRpY2FsTmF2PlxuICAgICAgIDxDYWxlbmRhciAvPlxuICAgICAgICA8c3R5bGUganN4PntgXG4gICAgICAgICAgLmNhbGVuZGVyU3R5bGUge1xuICAgICAgICAgICAgaGVpZ2h0OiA5MHZoO1xuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgICAgfVxuICAgICAgICBgfTwvc3R5bGU+XG4gICAgICA8L1ZlcnRpY2FsTmF2PlxuICAgIClcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBDb25zb2xlO1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/pages/console.js */",
        __self: this
      }));
    }
  }]);

  return Console;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Console);
    (function (Component, route) {
      if(!Component) return
      if (false) {}
      module.hot.accept()
      Component.__route = route

      if (module.hot.status() === 'idle') return

      var components = next.router.components
      for (var r in components) {
        if (!components.hasOwnProperty(r)) continue

        if (components[r].Component.__route === route) {
          next.router.update(r, Component)
        }
      }
    })(typeof __webpack_exports__ !== 'undefined' ? __webpack_exports__.default : (module.exports.default || module.exports), "/console")
  
/* WEBPACK VAR INJECTION */}.call(this, __webpack_require__(/*! ./../node_modules/webpack/buildin/harmony-module.js */ "./node_modules/webpack/buildin/harmony-module.js")(module)))

/***/ })

})
//# sourceMappingURL=console.js.c66b934522666dfc3dd9.hot-update.js.map