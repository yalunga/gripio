webpackHotUpdate("static/development/pages/console.js",{

/***/ "./components/Calendar.js":
/*!********************************!*\
  !*** ./components/Calendar.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "./node_modules/styled-jsx/style.js");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "./node_modules/react/index.js");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _AddWodModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddWodModal */ "./components/AddWodModal.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-big-calendar */ "./node_modules/react-big-calendar/lib/index.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "./node_modules/moment/moment.js");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-big-calendar/lib/css/react-big-calendar.css */ "./node_modules/react-big-calendar/lib/css/react-big-calendar.css");
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../firebase */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "./node_modules/next/router.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/andyyalung/gripio/components/Calendar.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var localizer = react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a.momentLocalizer(moment__WEBPACK_IMPORTED_MODULE_4___default.a);

var Calendar =
/*#__PURE__*/
function (_Component) {
  _inherits(Calendar, _Component);

  function Calendar() {
    var _this;

    _classCallCheck(this, Calendar);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Calendar).call(this));
    _this.state = {
      events: [],
      modalActive: false
    };
    _this.onSelectWOD = _this.onSelectWOD.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.modal = null;
    return _this;
  }

  _createClass(Calendar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var user = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().currentUser;

      if (user == null) {
        next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push('/signup');
        return;
      }

      var wodRef = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].firestore().collection('users').doc(user.uid).collection('wods');
      var getWods = wodRef.get().then(function (snapshot) {
        var events = [];
        snapshot.forEach(function (doc) {
          console.log(doc.id, '=>', doc.data());
          var event = {
            start: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            end: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            title: doc.data().wodTitle,
            allDay: true,
            resource: doc.data()
          };
          events.push(event);
        });
        this.setState({
          events: events
        });
      }.bind(this)).catch(function (err) {
        console.log('Error getting documents', err);
      });
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "onSelectWOD",
    value: function onSelectWOD(e) {
      console.log(e);
      this.modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddWodModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handler: this.handleModal,
        date: e.start,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      });
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.events);
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }, this.state.modalActive ? this.modal : '', react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-2494239222" + " " + "hero",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "hero-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a, {
        localizer: localizer,
        events: this.state.events,
        startAccessor: "start",
        endAccessor: "end",
        views: {
          month: true,
          agenda: true
        },
        selectable: true,
        style: {
          height: '90vh'
        },
        onSelectEvent: this.onSelectWOD,
        onSelectSlot: this.onSelectWOD,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      })))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2494239222",
        css: ".calenderStyle.jsx-2494239222{height:90vh;float:left;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXVGb0IsQUFHeUIsWUFDRCxXQUNiIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBBZGRXb2RNb2RhbCBmcm9tICcuL0FkZFdvZE1vZGFsJ1xuaW1wb3J0IEJpZ0NhbGVuZGFyIGZyb20gJ3JlYWN0LWJpZy1jYWxlbmRhcidcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50J1xuaW1wb3J0IFwicmVhY3QtYmlnLWNhbGVuZGFyL2xpYi9jc3MvcmVhY3QtYmlnLWNhbGVuZGFyLmNzc1wiO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcblxuXG5cbmNvbnN0IGxvY2FsaXplciA9IEJpZ0NhbGVuZGFyLm1vbWVudExvY2FsaXplcihtb21lbnQpXG5cblxuY2xhc3MgQ2FsZW5kYXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGV2ZW50czogW10sXG4gICAgICBtb2RhbEFjdGl2ZTogZmFsc2UsXG4gICAgfVxuICAgIHRoaXMub25TZWxlY3RXT0QgPSB0aGlzLm9uU2VsZWN0V09ELmJpbmQodGhpcylcbiAgICB0aGlzLmhhbmRsZU1vZGFsID0gdGhpcy5oYW5kbGVNb2RhbC5iaW5kKHRoaXMpXG4gICAgdGhpcy5tb2RhbCA9IG51bGw7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBjb25zdCB1c2VyID0gZmIuYXV0aCgpLmN1cnJlbnRVc2VyO1xuICAgIGlmKHVzZXIgPT0gbnVsbCkge1xuICAgICAgUm91dGVyLnB1c2goJy9zaWdudXAnKVxuICAgICAgcmV0dXJuO1xuICAgIH0gXG4gICAgY29uc3Qgd29kUmVmID0gZmIuZmlyZXN0b3JlKCkuY29sbGVjdGlvbigndXNlcnMnKS5kb2ModXNlci51aWQpLmNvbGxlY3Rpb24oJ3dvZHMnKTtcbiAgICBsZXQgZ2V0V29kcyA9IHdvZFJlZi5nZXQoKVxuICAgICAgLnRoZW4oZnVuY3Rpb24oc25hcHNob3QpIHtcbiAgICAgICAgbGV0IGV2ZW50cyA9IFtdO1xuICAgICAgICBzbmFwc2hvdC5mb3JFYWNoKGZ1bmN0aW9uKGRvYykge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGRvYy5pZCwgJz0+JywgZG9jLmRhdGEoKSk7XG4gICAgICAgICAgY29uc3QgZXZlbnQgPSB7XG4gICAgICAgICAgICBzdGFydDogbmV3IG1vbWVudChkb2MuZGF0YSgpLmRhdGUuc2Vjb25kcyAqIDEwMDApLFxuICAgICAgICAgICAgZW5kOiBuZXcgbW9tZW50KGRvYy5kYXRhKCkuZGF0ZS5zZWNvbmRzICogMTAwMCksXG4gICAgICAgICAgICB0aXRsZTogZG9jLmRhdGEoKS53b2RUaXRsZSxcbiAgICAgICAgICAgIGFsbERheTogdHJ1ZSxcbiAgICAgICAgICAgIHJlc291cmNlOiBkb2MuZGF0YSgpXG4gICAgICAgICAgfVxuICAgICAgICAgIGV2ZW50cy5wdXNoKGV2ZW50KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBldmVudHMgfSlcbiAgICAgIH0uYmluZCh0aGlzKSkuY2F0Y2goZXJyID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ0Vycm9yIGdldHRpbmcgZG9jdW1lbnRzJywgZXJyKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgaGFuZGxlTW9kYWwoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7bW9kYWxBY3RpdmU6IGZhbHNlfSlcbiAgfVxuXG4gIG9uU2VsZWN0V09EKGUpIHtcbiAgICBjb25zb2xlLmxvZyhlKVxuICAgIHRoaXMubW9kYWwgPSA8QWRkV29kTW9kYWwgaGFuZGxlcj17dGhpcy5oYW5kbGVNb2RhbH0gZGF0ZT17ZS5zdGFydH0gLz5cbiAgICB0aGlzLnNldFN0YXRlKHsgbW9kYWxBY3RpdmU6IHRydWUgfSlcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLmV2ZW50cylcbiAgICByZXR1cm4oXG4gICAgICA8ZGl2PlxuICAgICAgICB7KHRoaXMuc3RhdGUubW9kYWxBY3RpdmUpID8gdGhpcy5tb2RhbCA6ICcnfVxuICAgICAgICA8c2VjdGlvbiBjbGFzc05hbWU9XCJoZXJvXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZXJvLWJvZHlcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgIDxCaWdDYWxlbmRhclxuICAgICAgICAgICAgICBsb2NhbGl6ZXI9e2xvY2FsaXplcn1cbiAgICAgICAgICAgICAgZXZlbnRzPXt0aGlzLnN0YXRlLmV2ZW50c31cbiAgICAgICAgICAgICAgc3RhcnRBY2Nlc3Nvcj1cInN0YXJ0XCJcbiAgICAgICAgICAgICAgZW5kQWNjZXNzb3I9XCJlbmRcIlxuICAgICAgICAgICAgICB2aWV3cz17e1xuICAgICAgICAgICAgICAgIG1vbnRoOiB0cnVlLFxuICAgICAgICAgICAgICAgIGFnZW5kYTogdHJ1ZVxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBzZWxlY3RhYmxlXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogJzkwdmgnfX1cbiAgICAgICAgICAgICAgb25TZWxlY3RFdmVudD17dGhpcy5vblNlbGVjdFdPRH1cbiAgICAgICAgICAgICAgb25TZWxlY3RTbG90PXt0aGlzLm9uU2VsZWN0V09EfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgIC5jYWxlbmRlclN0eWxlIHtcbiAgICAgICAgICAgIGhlaWdodDogOTB2aDtcbiAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICAgIH1cbiAgICAgICAgYH08L3N0eWxlPlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENhbGVuZGFyO1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/components/Calendar.js */",
        __self: this
      }));
    }
  }]);

  return Calendar;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Calendar);

/***/ })

})
//# sourceMappingURL=console.js.224710bc30482d1019e2.hot-update.js.map