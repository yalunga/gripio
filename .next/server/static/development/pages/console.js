module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 3);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/AddWodModal.js":
/*!***********************************!*\
  !*** ./components/AddWodModal.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _firebase_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../firebase.js */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! uuid/v4 */ "uuid/v4");
/* harmony import */ var uuid_v4__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(uuid_v4__WEBPACK_IMPORTED_MODULE_4__);
var _jsxFileName = "/Users/andyyalung/gripio/components/AddWodModal.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var AddWodModal =
/*#__PURE__*/
function (_Component) {
  _inherits(AddWodModal, _Component);

  function AddWodModal() {
    var _this;

    _classCallCheck(this, AddWodModal);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(AddWodModal).call(this));
    _this.state = {
      wodTitle: '',
      wodText: '',
      otherText: '',
      wodID: ''
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSave = _this.handleSave.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(AddWodModal, [{
    key: "handleChange",
    value: function handleChange(e) {
      this.setState(_defineProperty({}, e.target.name, e.target.value));
    }
  }, {
    key: "handleSave",
    value: function handleSave() {
      var userId = _firebase_js__WEBPACK_IMPORTED_MODULE_2__["default"].auth().currentUser.uid;
      var UUID = uuid_v4__WEBPACK_IMPORTED_MODULE_4___default()();
      _firebase_js__WEBPACK_IMPORTED_MODULE_2__["default"].firestore().collection('users').doc(userId).collection('wods').add({
        wodID: UUID,
        wodTitle: this.state.wodTitle,
        wodText: this.state.wodText,
        otherText: this.state.otherText,
        date: this.props.date
      }).then(function () {
        this.props.handler();
      }.bind(this));
    }
  }, {
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "modal is-active",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "modal-background",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "modal-card",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
        className: "jsx-2907674307" + " " + "modal-card-head",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-2907674307" + " " + "modal-card-title has-text-info",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      }, "Workout"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        "aria-label": "close",
        onClick: this.props.handler,
        className: "jsx-2907674307" + " " + "delete",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-2907674307" + " " + "modal-card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-2907674307" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 48
        },
        __self: this
      }, "Title"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "control",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 49
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "wodTitle",
        type: "text",
        placeholder: "Fran",
        value: this.state.wodTitle,
        onChange: this.handleChange,
        className: "jsx-2907674307" + " " + 'input wod-title',
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "select",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("select", {
        className: "jsx-2907674307",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        className: "jsx-2907674307",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 61
        },
        __self: this
      }, "For Time"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        className: "jsx-2907674307",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 62
        },
        __self: this
      }, "AMRAP"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        className: "jsx-2907674307",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 63
        },
        __self: this
      }, "EMOM"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("option", {
        className: "jsx-2907674307",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 64
        },
        __self: this
      }, "Strength")))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-2907674307" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-2907674307" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, "Workout"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "control",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("textarea", {
        name: "wodText",
        type: "text",
        placeholder: "21-15-9 Thrusters and Pullups",
        value: this.state.wodText,
        onChange: this.handleChange,
        className: "jsx-2907674307" + " " + 'textarea has-fixed-size',
        __source: {
          fileName: _jsxFileName,
          lineNumber: 74
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-2907674307" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-2907674307" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, "Additional Details (Optional)"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2907674307" + " " + "control",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("textarea", {
        name: "otherText",
        type: "text",
        placeholder: "You'll want to break into managable sets. Breathe at the top of each thruster.",
        value: this.state.otherText,
        onChange: this.handleChange,
        className: "jsx-2907674307" + " " + 'textarea has-fixed-size',
        __source: {
          fileName: _jsxFileName,
          lineNumber: 90
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-2907674307" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("footer", {
        className: "jsx-2907674307" + " " + "modal-card-foot",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 104
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: this.handleSave,
        className: "jsx-2907674307" + " " + "button is-success is-outlined",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 105
        },
        __self: this
      }, "Save"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: this.props.handler,
        className: "jsx-2907674307" + " " + "button is-danger is-outlined",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }, "Cancel"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2907674307",
        css: ".modal-card.jsx-2907674307{width:640px;}.wod-title.jsx-2907674307{width:70%;margin-right:10%;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0FkZFdvZE1vZGFsLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQWdIb0IsQUFHMkIsQUFHRixVQUNPLEVBSG5CLGVBSUEiLCJmaWxlIjoiL1VzZXJzL2FuZHl5YWx1bmcvZ3JpcGlvL2NvbXBvbmVudHMvQWRkV29kTW9kYWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlLmpzJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcbmltcG9ydCB1dWlkdjQgZnJvbSAndXVpZC92NCdcblxuY2xhc3MgQWRkV29kTW9kYWwgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIHdvZFRpdGxlOiAnJyxcbiAgICAgIHdvZFRleHQ6ICcnLFxuICAgICAgb3RoZXJUZXh0OiAnJyxcbiAgICAgIHdvZElEOiAnJ1xuICAgIH1cbiAgICB0aGlzLmhhbmRsZUNoYW5nZSA9IHRoaXMuaGFuZGxlQ2hhbmdlLmJpbmQodGhpcylcbiAgICB0aGlzLmhhbmRsZVNhdmUgPSB0aGlzLmhhbmRsZVNhdmUuYmluZCh0aGlzKVxuICB9XG5cbiAgaGFuZGxlQ2hhbmdlKGUpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgW2UudGFyZ2V0Lm5hbWVdOiBlLnRhcmdldC52YWx1ZSB9KTtcbiAgfVxuXG4gIGhhbmRsZVNhdmUoKSB7XG4gICAgY29uc3QgdXNlcklkID0gZmIuYXV0aCgpLmN1cnJlbnRVc2VyLnVpZDtcbiAgICBjb25zdCBVVUlEID0gdXVpZHY0KCk7XG4gICAgZmIuZmlyZXN0b3JlKCkuY29sbGVjdGlvbigndXNlcnMnKS5kb2ModXNlcklkKS5jb2xsZWN0aW9uKCd3b2RzJykuYWRkKHtcbiAgICAgIHdvZElEOiBVVUlELFxuICAgICAgd29kVGl0bGU6IHRoaXMuc3RhdGUud29kVGl0bGUsXG4gICAgICB3b2RUZXh0OiB0aGlzLnN0YXRlLndvZFRleHQsXG4gICAgICBvdGhlclRleHQ6IHRoaXMuc3RhdGUub3RoZXJUZXh0LFxuICAgICAgZGF0ZTogdGhpcy5wcm9wcy5kYXRlXG4gICAgfSkudGhlbihmdW5jdGlvbigpIHtcbiAgICAgIHRoaXMucHJvcHMuaGFuZGxlcigpO1xuICAgIH0uYmluZCh0aGlzKSk7XG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgcmV0dXJuKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbCBpcy1hY3RpdmVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbC1iYWNrZ3JvdW5kXCI+PC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtY2FyZFwiPlxuICAgICAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPVwibW9kYWwtY2FyZC1oZWFkXCI+XG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJtb2RhbC1jYXJkLXRpdGxlIGhhcy10ZXh0LWluZm9cIj5Xb3Jrb3V0PC9wPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJkZWxldGVcIiBhcmlhLWxhYmVsPVwiY2xvc2VcIiBvbkNsaWNrPXt0aGlzLnByb3BzLmhhbmRsZXJ9PjwvYnV0dG9uPlxuICAgICAgICAgIDwvaGVhZGVyPlxuICAgICAgICAgIDxzZWN0aW9uIGNsYXNzTmFtZT1cIm1vZGFsLWNhcmQtYm9keVwiPlxuICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImxhYmVsXCI+VGl0bGU8L2xhYmVsPlxuICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJjb250cm9sXCI+XG4gICAgICAgICAgICAgICAgIDxpbnB1dFxuICAgICAgICAgICAgICAgICAgIG5hbWU9XCJ3b2RUaXRsZVwiXG4gICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPSdpbnB1dCB3b2QtdGl0bGUnXG4gICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiRnJhblwiXG4gICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUud29kVGl0bGV9XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuXG4gICAgICAgICAgICAgICAgIC8+XG4gICAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwic2VsZWN0XCI+XG4gICAgICAgICAgICAgICAgICAgIDxzZWxlY3Q+XG4gICAgICAgICAgICAgICAgICAgICAgPG9wdGlvbj5Gb3IgVGltZTwvb3B0aW9uPlxuICAgICAgICAgICAgICAgICAgICAgIDxvcHRpb24+QU1SQVA8L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPkVNT008L29wdGlvbj5cbiAgICAgICAgICAgICAgICAgICAgICA8b3B0aW9uPlN0cmVuZ3RoPC9vcHRpb24+XG4gICAgICAgICAgICAgICAgICAgIDwvc2VsZWN0PlxuICAgICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImhlbHAgaXMtZGFuZ2VyXCI+PC9wPlxuICAgICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJmaWVsZFwiPlxuICAgICAgICAgICAgICAgPGxhYmVsIGNsYXNzTmFtZT1cImxhYmVsXCI+V29ya291dDwvbGFiZWw+XG4gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRyb2xcIj5cbiAgICAgICAgICAgICAgICAgPHRleHRhcmVhXG4gICAgICAgICAgICAgICAgICAgbmFtZT1cIndvZFRleHRcIlxuICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT0ndGV4dGFyZWEgaGFzLWZpeGVkLXNpemUnXG4gICAgICAgICAgICAgICAgICAgdHlwZT1cInRleHRcIlxuICAgICAgICAgICAgICAgICAgIHBsYWNlaG9sZGVyPVwiMjEtMTUtOSBUaHJ1c3RlcnMgYW5kIFB1bGx1cHNcIlxuICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLndvZFRleHR9XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuXG4gICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgICAgPHAgY2xhc3NOYW1lPVwiaGVscCBpcy1kYW5nZXJcIj48L3A+XG4gICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwibGFiZWxcIj5BZGRpdGlvbmFsIERldGFpbHMgKE9wdGlvbmFsKTwvbGFiZWw+XG4gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRyb2xcIj5cbiAgICAgICAgICAgICAgICAgPHRleHRhcmVhXG4gICAgICAgICAgICAgICAgICAgbmFtZT1cIm90aGVyVGV4dFwiXG4gICAgICAgICAgICAgICAgICAgY2xhc3NOYW1lPSd0ZXh0YXJlYSBoYXMtZml4ZWQtc2l6ZSdcbiAgICAgICAgICAgICAgICAgICB0eXBlPVwidGV4dFwiXG4gICAgICAgICAgICAgICAgICAgcGxhY2Vob2xkZXI9XCJZb3UnbGwgd2FudCB0byBicmVhayBpbnRvIG1hbmFnYWJsZSBzZXRzLiBCcmVhdGhlIGF0IHRoZSB0b3Agb2YgZWFjaCB0aHJ1c3Rlci5cIlxuICAgICAgICAgICAgICAgICAgIHZhbHVlPXt0aGlzLnN0YXRlLm90aGVyVGV4dH1cbiAgICAgICAgICAgICAgICAgICBvbkNoYW5nZT17dGhpcy5oYW5kbGVDaGFuZ2V9XG5cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJoZWxwIGlzLWRhbmdlclwiPjwvcD5cbiAgICAgICAgICAgICA8L2Rpdj5cblxuICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICA8Zm9vdGVyIGNsYXNzTmFtZT1cIm1vZGFsLWNhcmQtZm9vdFwiPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b24gaXMtc3VjY2VzcyBpcy1vdXRsaW5lZFwiIG9uQ2xpY2s9e3RoaXMuaGFuZGxlU2F2ZX0+XG4gICAgICAgICAgICAgIFNhdmVcbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b24gaXMtZGFuZ2VyIGlzLW91dGxpbmVkXCIgb25DbGljaz17dGhpcy5wcm9wcy5oYW5kbGVyfT5cbiAgICAgICAgICAgICAgQ2FuY2VsXG4gICAgICAgICAgICA8L2J1dHRvbj5cbiAgICAgICAgICA8L2Zvb3Rlcj5cbiAgICAgICAgPC9kaXY+XG4gICAgICAgIDxzdHlsZSBqc3g+e2BcbiAgICAgICAgICAgIC5tb2RhbC1jYXJkIHtcbiAgICAgICAgICAgICAgd2lkdGg6IDY0MHB4O1xuICAgICAgICAgICAgfVxuICAgICAgICAgICAgLndvZC10aXRsZSB7XG4gICAgICAgICAgICAgIHdpZHRoOiA3MCU7XG4gICAgICAgICAgICAgIG1hcmdpbi1yaWdodDogMTAlO1xuICAgICAgICAgICAgfVxuICAgICAgICAgIGB9XG4gICAgICAgIDwvc3R5bGU+XG4gICAgICA8L2Rpdj5cbiAgICApXG4gIH1cbn1cblxuZXhwb3J0IGRlZmF1bHQgQWRkV29kTW9kYWw7XG4iXX0= */\n/*@ sourceURL=/Users/andyyalung/gripio/components/AddWodModal.js */",
        __self: this
      }));
    }
  }]);

  return AddWodModal;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (AddWodModal);

/***/ }),

/***/ "./components/Calendar.js":
/*!********************************!*\
  !*** ./components/Calendar.js ***!
  \********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _AddWodModal__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./AddWodModal */ "./components/AddWodModal.js");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! react-big-calendar */ "react-big-calendar");
/* harmony import */ var react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! moment */ "moment");
/* harmony import */ var moment__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(moment__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! react-big-calendar/lib/css/react-big-calendar.css */ "./node_modules/react-big-calendar/lib/css/react-big-calendar.css");
/* harmony import */ var react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(react_big_calendar_lib_css_react_big_calendar_css__WEBPACK_IMPORTED_MODULE_5__);
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../firebase */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/andyyalung/gripio/components/Calendar.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }








var localizer = react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a.momentLocalizer(moment__WEBPACK_IMPORTED_MODULE_4___default.a);

var Calendar =
/*#__PURE__*/
function (_Component) {
  _inherits(Calendar, _Component);

  function Calendar() {
    var _this;

    _classCallCheck(this, Calendar);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Calendar).call(this));
    _this.state = {
      events: [],
      modalActive: false
    };
    _this.onSelectWOD = _this.onSelectWOD.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.modal = null;
    return _this;
  }

  _createClass(Calendar, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var user = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].auth().currentUser;

      if (user == null) {
        next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push('/signup');
        return;
      }

      var wodRef = _firebase__WEBPACK_IMPORTED_MODULE_6__["default"].firestore().collection('users').doc(user.uid).collection('wods');
      var getWods = wodRef.get().then(function (snapshot) {
        var events = [];
        snapshot.forEach(function (doc) {
          console.log(doc.id, '=>', doc.data());
          var event = {
            start: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            end: new moment__WEBPACK_IMPORTED_MODULE_4___default.a(doc.data().date.seconds * 1000),
            title: doc.data().wodTitle,
            allDay: true,
            resource: doc.data()
          };
          events.push(event);
        });
        this.setState({
          events: events
        });
      }.bind(this)).catch(function (err) {
        console.log('Error getting documents', err);
      });
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "onSelectWOD",
    value: function onSelectWOD(e) {
      console.log(e);
      this.modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_AddWodModal__WEBPACK_IMPORTED_MODULE_2__["default"], {
        handler: this.handleModal,
        date: e.start,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      });
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.events);
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 66
        },
        __self: this
      }, this.state.modalActive ? this.modal : '', react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-2494239222" + " " + "hero",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "hero-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-2494239222" + " " + "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(react_big_calendar__WEBPACK_IMPORTED_MODULE_3___default.a, {
        localizer: localizer,
        events: this.state.events,
        startAccessor: "start",
        endAccessor: "end",
        views: {
          month: true,
          agenda: true
        },
        selectable: true,
        style: {
          height: '90vh'
        },
        onSelectEvent: this.onSelectWOD,
        onSelectSlot: this.onSelectWOD,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      })))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2494239222",
        css: ".calenderStyle.jsx-2494239222{height:90vh;float:left;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXVGb0IsQUFHeUIsWUFDRCxXQUNiIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0NhbGVuZGFyLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBBZGRXb2RNb2RhbCBmcm9tICcuL0FkZFdvZE1vZGFsJ1xuaW1wb3J0IEJpZ0NhbGVuZGFyIGZyb20gJ3JlYWN0LWJpZy1jYWxlbmRhcidcbmltcG9ydCBtb21lbnQgZnJvbSAnbW9tZW50J1xuaW1wb3J0IFwicmVhY3QtYmlnLWNhbGVuZGFyL2xpYi9jc3MvcmVhY3QtYmlnLWNhbGVuZGFyLmNzc1wiO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcblxuXG5cbmNvbnN0IGxvY2FsaXplciA9IEJpZ0NhbGVuZGFyLm1vbWVudExvY2FsaXplcihtb21lbnQpXG5cblxuY2xhc3MgQ2FsZW5kYXIgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICAgIGV2ZW50czogW10sXG4gICAgICBtb2RhbEFjdGl2ZTogZmFsc2UsXG4gICAgfVxuICAgIHRoaXMub25TZWxlY3RXT0QgPSB0aGlzLm9uU2VsZWN0V09ELmJpbmQodGhpcylcbiAgICB0aGlzLmhhbmRsZU1vZGFsID0gdGhpcy5oYW5kbGVNb2RhbC5iaW5kKHRoaXMpXG4gICAgdGhpcy5tb2RhbCA9IG51bGw7XG4gIH1cblxuICBjb21wb25lbnREaWRNb3VudCgpIHtcbiAgICBjb25zdCB1c2VyID0gZmIuYXV0aCgpLmN1cnJlbnRVc2VyO1xuICAgIGlmKHVzZXIgPT0gbnVsbCkge1xuICAgICAgUm91dGVyLnB1c2goJy9zaWdudXAnKVxuICAgICAgcmV0dXJuO1xuICAgIH0gXG4gICAgY29uc3Qgd29kUmVmID0gZmIuZmlyZXN0b3JlKCkuY29sbGVjdGlvbigndXNlcnMnKS5kb2ModXNlci51aWQpLmNvbGxlY3Rpb24oJ3dvZHMnKTtcbiAgICBsZXQgZ2V0V29kcyA9IHdvZFJlZi5nZXQoKVxuICAgICAgLnRoZW4oZnVuY3Rpb24oc25hcHNob3QpIHtcbiAgICAgICAgbGV0IGV2ZW50cyA9IFtdO1xuICAgICAgICBzbmFwc2hvdC5mb3JFYWNoKGZ1bmN0aW9uKGRvYykge1xuICAgICAgICAgIGNvbnNvbGUubG9nKGRvYy5pZCwgJz0+JywgZG9jLmRhdGEoKSk7XG4gICAgICAgICAgY29uc3QgZXZlbnQgPSB7XG4gICAgICAgICAgICBzdGFydDogbmV3IG1vbWVudChkb2MuZGF0YSgpLmRhdGUuc2Vjb25kcyAqIDEwMDApLFxuICAgICAgICAgICAgZW5kOiBuZXcgbW9tZW50KGRvYy5kYXRhKCkuZGF0ZS5zZWNvbmRzICogMTAwMCksXG4gICAgICAgICAgICB0aXRsZTogZG9jLmRhdGEoKS53b2RUaXRsZSxcbiAgICAgICAgICAgIGFsbERheTogdHJ1ZSxcbiAgICAgICAgICAgIHJlc291cmNlOiBkb2MuZGF0YSgpXG4gICAgICAgICAgfVxuICAgICAgICAgIGV2ZW50cy5wdXNoKGV2ZW50KTtcbiAgICAgICAgfSk7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBldmVudHMgfSlcbiAgICAgIH0uYmluZCh0aGlzKSkuY2F0Y2goZXJyID0+IHtcbiAgICAgICAgY29uc29sZS5sb2coJ0Vycm9yIGdldHRpbmcgZG9jdW1lbnRzJywgZXJyKTtcbiAgICAgIH0pO1xuICB9XG5cbiAgaGFuZGxlTW9kYWwoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7bW9kYWxBY3RpdmU6IGZhbHNlfSlcbiAgfVxuXG4gIG9uU2VsZWN0V09EKGUpIHtcbiAgICBjb25zb2xlLmxvZyhlKVxuICAgIHRoaXMubW9kYWwgPSA8QWRkV29kTW9kYWwgaGFuZGxlcj17dGhpcy5oYW5kbGVNb2RhbH0gZGF0ZT17ZS5zdGFydH0gLz5cbiAgICB0aGlzLnNldFN0YXRlKHsgbW9kYWxBY3RpdmU6IHRydWUgfSlcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBjb25zb2xlLmxvZyh0aGlzLnN0YXRlLmV2ZW50cylcbiAgICByZXR1cm4oXG4gICAgICA8ZGl2PlxuICAgICAgICB7KHRoaXMuc3RhdGUubW9kYWxBY3RpdmUpID8gdGhpcy5tb2RhbCA6ICcnfVxuICAgICAgICA8c2VjdGlvbiBjbGFzc05hbWU9XCJoZXJvXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJoZXJvLWJvZHlcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udGFpbmVyXCI+XG4gICAgICAgICAgICAgIDxCaWdDYWxlbmRhclxuICAgICAgICAgICAgICBsb2NhbGl6ZXI9e2xvY2FsaXplcn1cbiAgICAgICAgICAgICAgZXZlbnRzPXt0aGlzLnN0YXRlLmV2ZW50c31cbiAgICAgICAgICAgICAgc3RhcnRBY2Nlc3Nvcj1cInN0YXJ0XCJcbiAgICAgICAgICAgICAgZW5kQWNjZXNzb3I9XCJlbmRcIlxuICAgICAgICAgICAgICB2aWV3cz17e1xuICAgICAgICAgICAgICAgIG1vbnRoOiB0cnVlLFxuICAgICAgICAgICAgICAgIGFnZW5kYTogdHJ1ZVxuICAgICAgICAgICAgICB9fVxuICAgICAgICAgICAgICBzZWxlY3RhYmxlXG4gICAgICAgICAgICAgIHN0eWxlPXt7IGhlaWdodDogJzkwdmgnfX1cbiAgICAgICAgICAgICAgb25TZWxlY3RFdmVudD17dGhpcy5vblNlbGVjdFdPRH1cbiAgICAgICAgICAgICAgb25TZWxlY3RTbG90PXt0aGlzLm9uU2VsZWN0V09EfVxuICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgPC9kaXY+XG4gICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgIC5jYWxlbmRlclN0eWxlIHtcbiAgICAgICAgICAgIGhlaWdodDogOTB2aDtcbiAgICAgICAgICAgIGZsb2F0OiBsZWZ0O1xuICAgICAgICAgIH1cbiAgICAgICAgYH08L3N0eWxlPlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59XG5cbmV4cG9ydCBkZWZhdWx0IENhbGVuZGFyO1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/components/Calendar.js */",
        __self: this
      }));
    }
  }]);

  return Calendar;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Calendar);

/***/ }),

/***/ "./components/LoginModal.js":
/*!**********************************!*\
  !*** ./components/LoginModal.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _firebase_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../firebase.js */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/Users/andyyalung/gripio/components/LoginModal.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }





var LoginModal =
/*#__PURE__*/
function (_Component) {
  _inherits(LoginModal, _Component);

  function LoginModal() {
    var _this;

    _classCallCheck(this, LoginModal);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LoginModal).call(this));
    _this.state = {
      email: '',
      password: '',
      emailHelpText: '',
      passwordHelpText: ''
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleLogin = _this.handleLogin.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEmailFocus = _this.handleEmailFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handlePasswordFocus = _this.handlePasswordFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(LoginModal, [{
    key: "handleChange",
    value: function handleChange(e) {
      this.setState(_defineProperty({}, e.target.name, e.target.value));
    }
  }, {
    key: "handleLogin",
    value: function handleLogin() {
      var _this2 = this;

      _firebase_js__WEBPACK_IMPORTED_MODULE_2__["default"].auth().signInWithEmailAndPassword(this.state.email, this.state.password).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;

        if (errorCode == 'auth/invalid-email' || errorCode == 'auth/user-not-found') {
          this.setState({
            emailHelpText: 'Email not found.'
          });
        }

        if (errorCode == 'auth/wrong-password') {
          this.setState({
            passwordHelpText: 'The password is invalid.'
          });
        }
      }.bind(this)).then(function () {
        var user = _firebase_js__WEBPACK_IMPORTED_MODULE_2__["default"].auth().currentUser;

        if (user) {
          _this2.props.handler();

          next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push('/console');
        }
      });
    }
  }, {
    key: "handleEmailFocus",
    value: function handleEmailFocus() {
      this.setState({
        emailHelpText: ''
      });
    }
  }, {
    key: "handlePasswordFocus",
    value: function handlePasswordFocus() {
      this.setState({
        passwordHelpText: ''
      });
    }
  }, {
    key: "render",
    value: function render() {
      var emailStyle = ["input", "is-rounded"];
      var passwordStyle = ["input", "is-rounded"];

      if (this.state.emailHelpText != '') {
        emailStyle.push('is-danger');
      } else {
        emailStyle = ["input", "is-rounded"];
      }

      if (this.state.passwordHelpText != '') {
        passwordStyle.push('is-danger');
      } else {
        passwordStyle = ["input", "is-rounded"];
      }

      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "modal is-active",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "modal-background",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "modal-card",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
        className: "jsx-881260932" + " " + "modal-card-head",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-881260932" + " " + "modal-card-title has-text-info",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, "Login"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        "aria-label": "close",
        onClick: this.props.handler,
        className: "jsx-881260932" + " " + "delete",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-881260932" + " " + "modal-card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-881260932" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }, "Email"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "email",
        type: "email",
        placeholder: "e.g. alexsmith@gmail.com",
        value: this.state.email,
        onChange: this.handleChange,
        onFocus: this.handleEmailFocus,
        className: "jsx-881260932" + " " + (emailStyle.join(' ') || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "jsx-881260932" + " " + "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
        className: "jsx-881260932" + " " + "fas fa-envelope",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-881260932" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, this.state.emailHelpText)), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-881260932" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, "Password"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "password",
        type: "password",
        value: this.state.password,
        onChange: this.handleChange,
        onFocus: this.handlePasswordFocus,
        className: "jsx-881260932" + " " + (passwordStyle.join(' ') || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "jsx-881260932" + " " + "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
        className: "jsx-881260932" + " " + "fas fa-lock",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-881260932" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, this.state.passwordHelpText))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("footer", {
        className: "jsx-881260932" + " " + "modal-card-foot",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: this.handleLogin,
        className: "jsx-881260932" + " " + "button is-info is-outlined",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, "Login"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: this.props.handler,
        className: "jsx-881260932" + " " + "button is-danger is-outlined",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, "Cancel"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "881260932",
        css: ".modal-card.jsx-881260932{width:500px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0xvZ2luTW9kYWwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBeUhvQixBQUcyQixZQUNkIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0xvZ2luTW9kYWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlLmpzJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcblxuY2xhc3MgTG9naW5Nb2RhbCBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKClcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgZW1haWw6ICcnLFxuICAgICAgcGFzc3dvcmQ6ICcnLFxuICAgICAgZW1haWxIZWxwVGV4dDogJycsXG4gICAgICBwYXNzd29yZEhlbHBUZXh0OiAnJ1xuICAgIH1cbiAgICB0aGlzLmhhbmRsZUNoYW5nZSA9IHRoaXMuaGFuZGxlQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5oYW5kbGVMb2dpbiA9IHRoaXMuaGFuZGxlTG9naW4uYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUVtYWlsRm9jdXMgPSB0aGlzLmhhbmRsZUVtYWlsRm9jdXMuYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZVBhc3N3b3JkRm9jdXMgPSB0aGlzLmhhbmRsZVBhc3N3b3JkRm9jdXMuYmluZCh0aGlzKTtcbiAgfVxuXG4gIGhhbmRsZUNoYW5nZShlKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IFtlLnRhcmdldC5uYW1lXTogZS50YXJnZXQudmFsdWUgfSk7XG4gIH1cblxuICBoYW5kbGVMb2dpbigpIHtcbiAgICBmYi5hdXRoKCkuc2lnbkluV2l0aEVtYWlsQW5kUGFzc3dvcmQodGhpcy5zdGF0ZS5lbWFpbCwgdGhpcy5zdGF0ZS5wYXNzd29yZClcbiAgICAuY2F0Y2goZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgIGNvbnN0IGVycm9yQ29kZSA9IGVycm9yLmNvZGU7XG4gICAgICBjb25zdCBlcnJvck1lc3NhZ2UgPSBlcnJvci5tZXNzYWdlO1xuICAgICAgaWYoZXJyb3JDb2RlID09ICdhdXRoL2ludmFsaWQtZW1haWwnIHx8IGVycm9yQ29kZSA9PSAnYXV0aC91c2VyLW5vdC1mb3VuZCcpe1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZW1haWxIZWxwVGV4dDogJ0VtYWlsIG5vdCBmb3VuZC4nIH0pXG4gICAgICB9XG4gICAgICBpZihlcnJvckNvZGUgPT0gJ2F1dGgvd3JvbmctcGFzc3dvcmQnKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBwYXNzd29yZEhlbHBUZXh0OiAnVGhlIHBhc3N3b3JkIGlzIGludmFsaWQuJyB9KVxuICAgICAgfVxuICAgIH0uYmluZCh0aGlzKSkudGhlbigoKSA9PiB7XG4gICAgICBjb25zdCB1c2VyID0gZmIuYXV0aCgpLmN1cnJlbnRVc2VyO1xuICAgICAgaWYgKHVzZXIpIHtcbiAgICAgICAgdGhpcy5wcm9wcy5oYW5kbGVyKCk7XG4gICAgICAgIFJvdXRlci5wdXNoKCcvY29uc29sZScpXG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIGhhbmRsZUVtYWlsRm9jdXMoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVtYWlsSGVscFRleHQ6ICcnfSlcbiAgfVxuXG4gIGhhbmRsZVBhc3N3b3JkRm9jdXMoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IHBhc3N3b3JkSGVscFRleHQ6ICcnfSlcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBsZXQgZW1haWxTdHlsZSA9IFtcImlucHV0XCIsIFwiaXMtcm91bmRlZFwiXVxuICAgIGxldCBwYXNzd29yZFN0eWxlID0gW1wiaW5wdXRcIiwgXCJpcy1yb3VuZGVkXCJdXG5cbiAgICBpZih0aGlzLnN0YXRlLmVtYWlsSGVscFRleHQgIT0gJycpe1xuICAgICAgZW1haWxTdHlsZS5wdXNoKCdpcy1kYW5nZXInKVxuICAgIH0gZWxzZSB7XG4gICAgICBlbWFpbFN0eWxlID0gW1wiaW5wdXRcIiwgXCJpcy1yb3VuZGVkXCJdXG4gICAgfVxuICAgIGlmKHRoaXMuc3RhdGUucGFzc3dvcmRIZWxwVGV4dCAhPSAnJykge1xuICAgICAgcGFzc3dvcmRTdHlsZS5wdXNoKCdpcy1kYW5nZXInKVxuICAgIH0gZWxzZSB7XG4gICAgICBwYXNzd29yZFN0eWxlID0gW1wiaW5wdXRcIiwgXCJpcy1yb3VuZGVkXCJdXG4gICAgfVxuXG4gICAgcmV0dXJuKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbCBpcy1hY3RpdmVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbC1iYWNrZ3JvdW5kXCI+PC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtY2FyZFwiPlxuICAgICAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPVwibW9kYWwtY2FyZC1oZWFkXCI+XG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJtb2RhbC1jYXJkLXRpdGxlIGhhcy10ZXh0LWluZm9cIj5Mb2dpbjwvcD5cbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiZGVsZXRlXCIgYXJpYS1sYWJlbD1cImNsb3NlXCIgb25DbGljaz17dGhpcy5wcm9wcy5oYW5kbGVyfT48L2J1dHRvbj5cbiAgICAgICAgICA8L2hlYWRlcj5cbiAgICAgICAgICA8c2VjdGlvbiBjbGFzc05hbWU9XCJtb2RhbC1jYXJkLWJvZHlcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmllbGRcIj5cbiAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJsYWJlbFwiPkVtYWlsPC9sYWJlbD5cbiAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udHJvbCBoYXMtaWNvbnMtbGVmdFwiPlxuICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICBuYW1lPVwiZW1haWxcIlxuICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17ZW1haWxTdHlsZS5qb2luKCcgJyl9XG4gICAgICAgICAgICAgICAgICAgdHlwZT1cImVtYWlsXCJcbiAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cImUuZy4gYWxleHNtaXRoQGdtYWlsLmNvbVwiXG4gICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUuZW1haWx9XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMuaGFuZGxlRW1haWxGb2N1c31cbiAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiaWNvbiBpcy1zbWFsbCBpcy1sZWZ0XCI+XG4gICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWVudmVsb3BlXCI+PC9pPlxuICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImhlbHAgaXMtZGFuZ2VyXCI+e3RoaXMuc3RhdGUuZW1haWxIZWxwVGV4dH08L3A+XG4gICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwibGFiZWxcIj5QYXNzd29yZDwvbGFiZWw+XG4gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRyb2wgaGFzLWljb25zLWxlZnRcIj5cbiAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgbmFtZT1cInBhc3N3b3JkXCJcbiAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Bhc3N3b3JkU3R5bGUuam9pbignICcpfVxuICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXG4gICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGFzc3dvcmR9XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMuaGFuZGxlUGFzc3dvcmRGb2N1c31cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpY29uIGlzLXNtYWxsIGlzLWxlZnRcIj5cbiAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtbG9ja1wiPjwvaT5cbiAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJoZWxwIGlzLWRhbmdlclwiPnt0aGlzLnN0YXRlLnBhc3N3b3JkSGVscFRleHR9PC9wPlxuICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICA8Zm9vdGVyIGNsYXNzTmFtZT1cIm1vZGFsLWNhcmQtZm9vdFwiPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b24gaXMtaW5mbyBpcy1vdXRsaW5lZFwiIG9uQ2xpY2s9e3RoaXMuaGFuZGxlTG9naW59PlxuICAgICAgICAgICAgICBMb2dpblxuICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ1dHRvbiBpcy1kYW5nZXIgaXMtb3V0bGluZWRcIiBvbkNsaWNrPXt0aGlzLnByb3BzLmhhbmRsZXJ9PlxuICAgICAgICAgICAgICBDYW5jZWxcbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgIDwvZm9vdGVyPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgICAgLm1vZGFsLWNhcmQge1xuICAgICAgICAgICAgICB3aWR0aDogNTAwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgYH1cbiAgICAgICAgPC9zdHlsZT5cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBMb2dpbk1vZGFsO1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/components/LoginModal.js */",
        __self: this
      }));
    }
  }]);

  return LoginModal;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (LoginModal);

/***/ }),

/***/ "./components/VerticalNav.js":
/*!***********************************!*\
  !*** ./components/VerticalNav.js ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_styles_sass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/styles.sass */ "./styles/styles.sass");
/* harmony import */ var _styles_styles_sass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_styles_sass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _firebase_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../firebase.js */ "./firebase.js");
/* harmony import */ var _LoginModal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./LoginModal */ "./components/LoginModal.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/andyyalung/gripio/components/VerticalNav.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }









var VerticalNav =
/*#__PURE__*/
function (_Component) {
  _inherits(VerticalNav, _Component);

  function VerticalNav() {
    _classCallCheck(this, VerticalNav);

    return _possibleConstructorReturn(this, _getPrototypeOf(VerticalNav).call(this));
  }

  _createClass(VerticalNav, [{
    key: "render",
    value: function render() {
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 17
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 18
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
        charset: "utf-8",
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 19
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
        name: "viewport",
        content: "width=device-width, initial-scale=1",
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 20
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 21
        },
        __self: this
      }, "grip.io"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("script", {
        defer: true,
        src: "https://use.fontawesome.com/releases/v5.3.1/js/all.js",
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 22
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("aside", {
        className: "jsx-494876294" + " " + "menu menu-style",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 24
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_4___default.a, {
        href: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 25
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 26
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: '../static/grip_logo.png',
        width: "125",
        height: "60",
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 27
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-494876294" + " " + "menu-label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 30
        },
        __self: this
      }, "General"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
        className: "jsx-494876294" + " " + "menu-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 33
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 34
        },
        __self: this
      }, "Dashboard")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 35
        },
        __self: this
      }, "Customers"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-494876294" + " " + "menu-label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 37
        },
        __self: this
      }, "Administration"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
        className: "jsx-494876294" + " " + "menu-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 41
        },
        __self: this
      }, "Team Settings")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 42
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294" + " " + "is-active",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 43
        },
        __self: this
      }, "Manage Your Team"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 44
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 45
        },
        __self: this
      }, "Members")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 46
        },
        __self: this
      }, "Plugins")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 47
        },
        __self: this
      }, "Add a member")))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 50
        },
        __self: this
      }, "Invitations")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 51
        },
        __self: this
      }, "Cloud Storage Environment Settings")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 52
        },
        __self: this
      }, "Authentication"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-494876294" + " " + "menu-label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 54
        },
        __self: this
      }, "Transactions"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("ul", {
        className: "jsx-494876294" + " " + "menu-list",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 57
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 58
        },
        __self: this
      }, "Payments")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 59
        },
        __self: this
      }, "Transfers")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("li", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-494876294",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 60
        },
        __self: this
      }, "Balance")))), this.props.children, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "494876294",
        css: ".menu-style.jsx-494876294{float:left;width:15vw;margin-left:5px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL1ZlcnRpY2FsTmF2LmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQStEb0IsQUFHMkIsV0FDQyxXQUNLLGdCQUNwQiIsImZpbGUiOiIvVXNlcnMvYW5keXlhbHVuZy9ncmlwaW8vY29tcG9uZW50cy9WZXJ0aWNhbE5hdi5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgSGVhZCBmcm9tICduZXh0L2hlYWQnXG5pbXBvcnQgJy4uL3N0eWxlcy9zdHlsZXMuc2FzcydcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluaydcbmltcG9ydCBmYiBmcm9tICcuLi9maXJlYmFzZS5qcydcbmltcG9ydCBMb2dpbk1vZGFsIGZyb20gJy4vTG9naW5Nb2RhbCdcbmltcG9ydCBSb3V0ZXIgZnJvbSAnbmV4dC9yb3V0ZXInXG5cbmNsYXNzIFZlcnRpY2FsTmF2IGV4dGVuZHMgQ29tcG9uZW50IHtcblxuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpO1xuICB9XG5cbiAgcmVuZGVyKCkge1xuICAgIHJldHVybihcbiAgICAgIDxkaXY+XG4gICAgICAgIDxIZWFkPlxuICAgICAgICAgIDxtZXRhIGNoYXJzZXQ9XCJ1dGYtOFwiIC8+XG4gICAgICAgICAgPG1ldGEgbmFtZT1cInZpZXdwb3J0XCIgY29udGVudD1cIndpZHRoPWRldmljZS13aWR0aCwgaW5pdGlhbC1zY2FsZT0xXCIgLz5cbiAgICAgICAgICA8dGl0bGU+Z3JpcC5pbzwvdGl0bGU+XG4gICAgICAgICAgPHNjcmlwdCBkZWZlciBzcmM9XCJodHRwczovL3VzZS5mb250YXdlc29tZS5jb20vcmVsZWFzZXMvdjUuMy4xL2pzL2FsbC5qc1wiPjwvc2NyaXB0PlxuICAgICAgICA8L0hlYWQ+XG4gICAgICAgIDxhc2lkZSBjbGFzc05hbWU9XCJtZW51IG1lbnUtc3R5bGVcIj5cbiAgICAgICAgICAgIDxMaW5rIGhyZWY9XCIvXCI+XG4gICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm5hdmJhci1pdGVtXCI+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9eycuLi9zdGF0aWMvZ3JpcF9sb2dvLnBuZyd9IHdpZHRoPVwiMTI1XCIgaGVpZ2h0PVwiNjBcIi8+XG4gICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIm1lbnUtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICBHZW5lcmFsXG4gICAgICAgICAgICA8L3A+XG4gICAgICAgICAgICA8dWwgY2xhc3NOYW1lPVwibWVudS1saXN0XCI+XG4gICAgICAgICAgICAgICAgPGxpPjxhPkRhc2hib2FyZDwvYT48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT48YT5DdXN0b21lcnM8L2E+PC9saT5cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJtZW51LWxhYmVsXCI+XG4gICAgICAgICAgICAgICAgQWRtaW5pc3RyYXRpb25cbiAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJtZW51LWxpc3RcIj5cbiAgICAgICAgICAgICAgICA8bGk+PGE+VGVhbSBTZXR0aW5nczwvYT48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT5cbiAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJpcy1hY3RpdmVcIj5NYW5hZ2UgWW91ciBUZWFtPC9hPlxuICAgICAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICAgICAgPGxpPjxhPk1lbWJlcnM8L2E+PC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPjxhPlBsdWdpbnM8L2E+PC9saT5cbiAgICAgICAgICAgICAgICAgICAgPGxpPjxhPkFkZCBhIG1lbWJlcjwvYT48L2xpPlxuICAgICAgICAgICAgICAgIDwvdWw+XG4gICAgICAgICAgICAgICAgPC9saT5cbiAgICAgICAgICAgICAgICA8bGk+PGE+SW52aXRhdGlvbnM8L2E+PC9saT5cbiAgICAgICAgICAgICAgICA8bGk+PGE+Q2xvdWQgU3RvcmFnZSBFbnZpcm9ubWVudCBTZXR0aW5nczwvYT48L2xpPlxuICAgICAgICAgICAgICAgIDxsaT48YT5BdXRoZW50aWNhdGlvbjwvYT48L2xpPlxuICAgICAgICAgICAgPC91bD5cbiAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cIm1lbnUtbGFiZWxcIj5cbiAgICAgICAgICAgICAgICBUcmFuc2FjdGlvbnNcbiAgICAgICAgICAgIDwvcD5cbiAgICAgICAgICAgIDx1bCBjbGFzc05hbWU9XCJtZW51LWxpc3RcIj5cbiAgICAgICAgICAgICAgICA8bGk+PGE+UGF5bWVudHM8L2E+PC9saT5cbiAgICAgICAgICAgICAgICA8bGk+PGE+VHJhbnNmZXJzPC9hPjwvbGk+XG4gICAgICAgICAgICAgICAgPGxpPjxhPkJhbGFuY2U8L2E+PC9saT5cbiAgICAgICAgICAgIDwvdWw+XG4gICAgICAgIDwvYXNpZGU+XG4gICAgICAgIHt0aGlzLnByb3BzLmNoaWxkcmVufVxuICAgICAgICA8c3R5bGUganN4PntgXG4gICAgICAgICAgICAubWVudS1zdHlsZSB7XG4gICAgICAgICAgICAgICAgZmxvYXQ6bGVmdDtcbiAgICAgICAgICAgICAgICB3aWR0aDogMTV2dztcbiAgICAgICAgICAgICAgICBtYXJnaW4tbGVmdDogNXB4O1xuICAgICAgICAgICAgfVxuICAgICAgICBgfTwvc3R5bGU+XG4gICAgPC9kaXY+XG4gICAgKVxuICB9XG59XG5cblxuZXhwb3J0IGRlZmF1bHQgVmVydGljYWxOYXY7XG4iXX0= */\n/*@ sourceURL=/Users/andyyalung/gripio/components/VerticalNav.js */",
        __self: this
      }));
    }
  }]);

  return VerticalNav;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (VerticalNav);

/***/ }),

/***/ "./firebase.js":
/*!*********************!*\
  !*** ./firebase.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase */ "firebase");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_0__);

var config = {
  apiKey: "AIzaSyBnPR-8wZTA-UhJnqlotJZbZh96L7r1Sok",
  authDomain: "gripio-374d7.firebaseapp.com",
  databaseURL: "https://gripio-374d7.firebaseio.com",
  projectId: "gripio-374d7",
  storageBucket: "gripio-374d7.appspot.com",
  messagingSenderId: "940281243850"
};
/* harmony default export */ __webpack_exports__["default"] = (!firebase__WEBPACK_IMPORTED_MODULE_0___default.a.apps.length ? firebase__WEBPACK_IMPORTED_MODULE_0___default.a.initializeApp(config) : firebase__WEBPACK_IMPORTED_MODULE_0___default.a.app());

/***/ }),

/***/ "./node_modules/react-big-calendar/lib/css/react-big-calendar.css":
/*!************************************************************************!*\
  !*** ./node_modules/react-big-calendar/lib/css/react-big-calendar.css ***!
  \************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ "./pages/console.js":
/*!**************************!*\
  !*** ./pages/console.js ***!
  \**************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_VerticalNav__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/VerticalNav */ "./components/VerticalNav.js");
/* harmony import */ var _components_Calendar__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../components/Calendar */ "./components/Calendar.js");
/* harmony import */ var _firebase__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../firebase */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_5___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_5__);
var _jsxFileName = "/Users/andyyalung/gripio/pages/console.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }







var Console =
/*#__PURE__*/
function (_Component) {
  _inherits(Console, _Component);

  function Console() {
    var _this;

    _classCallCheck(this, Console);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Console).call(this));
    _this.state = {};
    _this.onSelectWOD = _this.onSelectWOD.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.modal = null;
    return _this;
  }

  _createClass(Console, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var user = _firebase__WEBPACK_IMPORTED_MODULE_4__["default"].auth().currentUser;

      if (user == null) {
        next_router__WEBPACK_IMPORTED_MODULE_5___default.a.push('/signup');
        return;
      }
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "onSelectWOD",
    value: function onSelectWOD(e) {
      console.log(e);
      this.modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(AddWodModal, {
        handler: this.handleModal,
        date: e.start,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 32
        },
        __self: this
      });
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "render",
    value: function render() {
      console.log(this.state.events);
      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_VerticalNav__WEBPACK_IMPORTED_MODULE_2__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 39
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_components_Calendar__WEBPACK_IMPORTED_MODULE_3__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 40
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "2494239222",
        css: ".calenderStyle.jsx-2494239222{height:90vh;float:left;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9wYWdlcy9jb25zb2xlLmpzIl0sIm5hbWVzIjpbXSwibWFwcGluZ3MiOiJBQXdDb0IsQUFHeUIsWUFDRCxXQUNiIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9wYWdlcy9jb25zb2xlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiaW1wb3J0IFJlYWN0LCB7IENvbXBvbmVudCB9IGZyb20gJ3JlYWN0JztcbmltcG9ydCBWZXJ0aWNhbE5hdiBmcm9tICcuLi9jb21wb25lbnRzL1ZlcnRpY2FsTmF2JztcbmltcG9ydCBDYWxlbmRhciBmcm9tICcuLi9jb21wb25lbnRzL0NhbGVuZGFyJztcbmltcG9ydCBmYiBmcm9tICcuLi9maXJlYmFzZSdcbmltcG9ydCBSb3V0ZXIgZnJvbSAnbmV4dC9yb3V0ZXInXG5cbmNsYXNzIENvbnNvbGUgZXh0ZW5kcyBDb21wb25lbnQge1xuICBjb25zdHJ1Y3RvcigpIHtcbiAgICBzdXBlcigpXG4gICAgdGhpcy5zdGF0ZSA9IHtcbiAgICBcbiAgICB9XG4gICAgdGhpcy5vblNlbGVjdFdPRCA9IHRoaXMub25TZWxlY3RXT0QuYmluZCh0aGlzKVxuICAgIHRoaXMuaGFuZGxlTW9kYWwgPSB0aGlzLmhhbmRsZU1vZGFsLmJpbmQodGhpcylcbiAgICB0aGlzLm1vZGFsID0gbnVsbDtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHVzZXIgPSBmYi5hdXRoKCkuY3VycmVudFVzZXI7XG4gICAgaWYodXNlciA9PSBudWxsKSB7XG4gICAgICBSb3V0ZXIucHVzaCgnL3NpZ251cCcpXG4gICAgICByZXR1cm47XG4gICAgfSBcbiAgfVxuXG4gIGhhbmRsZU1vZGFsKCkge1xuICAgIHRoaXMuc2V0U3RhdGUoe21vZGFsQWN0aXZlOiBmYWxzZX0pXG4gIH1cblxuICBvblNlbGVjdFdPRChlKSB7XG4gICAgY29uc29sZS5sb2coZSlcbiAgICB0aGlzLm1vZGFsID0gPEFkZFdvZE1vZGFsIGhhbmRsZXI9e3RoaXMuaGFuZGxlTW9kYWx9IGRhdGU9e2Uuc3RhcnR9IC8+XG4gICAgdGhpcy5zZXRTdGF0ZSh7IG1vZGFsQWN0aXZlOiB0cnVlIH0pXG4gIH1cblxuICByZW5kZXIoKSB7XG4gICAgY29uc29sZS5sb2codGhpcy5zdGF0ZS5ldmVudHMpXG4gICAgcmV0dXJuKFxuICAgICAgPFZlcnRpY2FsTmF2PlxuICAgICAgIDxDYWxlbmRhciAvPlxuICAgICAgICA8c3R5bGUganN4PntgXG4gICAgICAgICAgLmNhbGVuZGVyU3R5bGUge1xuICAgICAgICAgICAgaGVpZ2h0OiA5MHZoO1xuICAgICAgICAgICAgZmxvYXQ6IGxlZnQ7XG4gICAgICAgICAgfVxuICAgICAgICBgfTwvc3R5bGU+XG4gICAgICA8L1ZlcnRpY2FsTmF2PlxuICAgIClcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBDb25zb2xlO1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/pages/console.js */",
        __self: this
      }));
    }
  }]);

  return Console;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Console);

/***/ }),

/***/ "./styles/styles.sass":
/*!****************************!*\
  !*** ./styles/styles.sass ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 3:
/*!********************************!*\
  !*** multi ./pages/console.js ***!
  \********************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./pages/console.js */"./pages/console.js");


/***/ }),

/***/ "firebase":
/*!***************************!*\
  !*** external "firebase" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase");

/***/ }),

/***/ "moment":
/*!*************************!*\
  !*** external "moment" ***!
  \*************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("moment");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/link":
/*!****************************!*\
  !*** external "next/link" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "react-big-calendar":
/*!*************************************!*\
  !*** external "react-big-calendar" ***!
  \*************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react-big-calendar");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ }),

/***/ "uuid/v4":
/*!**************************!*\
  !*** external "uuid/v4" ***!
  \**************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("uuid/v4");

/***/ })

/******/ });
//# sourceMappingURL=console.js.map