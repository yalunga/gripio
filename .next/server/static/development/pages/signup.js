module.exports =
/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = require('../../../ssr-module-cache.js');
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		var threw = true;
/******/ 		try {
/******/ 			modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/ 			threw = false;
/******/ 		} finally {
/******/ 			if(threw) delete installedModules[moduleId];
/******/ 		}
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = 4);
/******/ })
/************************************************************************/
/******/ ({

/***/ "./components/Layout.js":
/*!******************************!*\
  !*** ./components/Layout.js ***!
  \******************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! next/head */ "next/head");
/* harmony import */ var next_head__WEBPACK_IMPORTED_MODULE_2___default = /*#__PURE__*/__webpack_require__.n(next_head__WEBPACK_IMPORTED_MODULE_2__);
/* harmony import */ var _styles_styles_sass__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../styles/styles.sass */ "./styles/styles.sass");
/* harmony import */ var _styles_styles_sass__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(_styles_styles_sass__WEBPACK_IMPORTED_MODULE_3__);
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! next/link */ "next/link");
/* harmony import */ var next_link__WEBPACK_IMPORTED_MODULE_4___default = /*#__PURE__*/__webpack_require__.n(next_link__WEBPACK_IMPORTED_MODULE_4__);
/* harmony import */ var _firebase_js__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../firebase.js */ "./firebase.js");
/* harmony import */ var _LoginModal__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./LoginModal */ "./components/LoginModal.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_7___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_7__);
var _jsxFileName = "/Users/andyyalung/gripio/components/Layout.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }









var Layout =
/*#__PURE__*/
function (_Component) {
  _inherits(Layout, _Component);

  function Layout() {
    var _this;

    _classCallCheck(this, Layout);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Layout).call(this));
    _this.state = {
      user: null,
      modalActive: false
    };
    _this.signOut = _this.signOut.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleModal = _this.handleModal.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.loginClicked = _this.loginClicked.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(Layout, [{
    key: "componentDidMount",
    value: function componentDidMount() {
      var user = _firebase_js__WEBPACK_IMPORTED_MODULE_5__["default"].auth().currentUser;

      if (user) {
        this.setState({
          user: user
        });
      }
    }
  }, {
    key: "signOut",
    value: function signOut() {
      _firebase_js__WEBPACK_IMPORTED_MODULE_5__["default"].auth().signOut().then(function () {
        this.setState({
          user: null
        });
        next_router__WEBPACK_IMPORTED_MODULE_7___default.a.push('/');
      }.bind(this)).catch(function (error) {// An error happened.
      });
    }
  }, {
    key: "loginClicked",
    value: function loginClicked() {
      this.setState({
        modalActive: true
      });
    }
  }, {
    key: "handleModal",
    value: function handleModal() {
      this.setState({
        modalActive: false
      });
    }
  }, {
    key: "render",
    value: function render() {
      var modal;
      var userButtons;

      if (this.state.user) {
        userButtons = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "buttons",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 51
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_4___default.a, {
          href: "/console",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 52
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
          className: "button is-info is-outlined",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 53
          },
          __self: this
        }, "Go To Console")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
          className: "button is-info is-outlined",
          onClick: this.signOut,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 57
          },
          __self: this
        }, "Sign Out"));
      } else {
        userButtons = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
          className: "buttons",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 64
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_4___default.a, {
          href: "/signup",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 65
          },
          __self: this
        }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
          className: "button is-info is-outlined",
          __source: {
            fileName: _jsxFileName,
            lineNumber: 66
          },
          __self: this
        }, "Sign up")), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
          className: "button is-info is-outlined",
          onClick: this.loginClicked,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 70
          },
          __self: this
        }, "Log in"));
      }

      if (this.state.modalActive) {
        modal = react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(_LoginModal__WEBPACK_IMPORTED_MODULE_6__["default"], {
          handler: this.handleModal,
          __source: {
            fileName: _jsxFileName,
            lineNumber: 78
          },
          __self: this
        });
      } else {
        modal = '';
      }

      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 84
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_head__WEBPACK_IMPORTED_MODULE_2___default.a, {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 85
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
        charset: "utf-8",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 86
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("meta", {
        name: "viewport",
        content: "width=device-width, initial-scale=1",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 87
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("title", {
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, "grip.io"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("script", {
        defer: true,
        src: "https://use.fontawesome.com/releases/v5.3.1/js/all.js",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      })), modal, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("nav", {
        role: "navigation",
        "aria-label": "main navigation",
        className: "jsx-1626315026" + " " + "navbar is-white",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026" + " " + "navbar-brand",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 93
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(next_link__WEBPACK_IMPORTED_MODULE_4___default.a, {
        href: "/",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 94
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("img", {
        src: '../static/grip_logo.png',
        width: "125",
        height: "60",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        role: "button",
        "aria-label": "menu",
        "aria-expanded": "false",
        "data-target": "navbarBasicExample",
        className: "jsx-1626315026" + " " + "navbar-burger burger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 100
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        "aria-hidden": "true",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 101
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        "aria-hidden": "true",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 102
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        "aria-hidden": "true",
        className: "jsx-1626315026",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 103
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        id: "navbarBasicExample",
        className: "jsx-1626315026" + " " + "navbar-menu",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026" + " " + "navbar-start",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 108
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }, "Home"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, "About"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026" + " " + "navbar-item has-dropdown is-hoverable",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-link",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, "More"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026" + " " + "navbar-dropdown",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 123
        },
        __self: this
      }, "About"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, "Jobs"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 129
        },
        __self: this
      }, "Contact"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("hr", {
        className: "jsx-1626315026" + " " + "navbar-divider",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 132
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("a", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 133
        },
        __self: this
      }, "Report an issue")))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026" + " " + "navbar-end",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 140
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-1626315026" + " " + "navbar-item",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 141
        },
        __self: this
      }, userButtons)))), this.props.children, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "1626315026",
        css: "\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0xheW91dC5qcyJdLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiQUFtSjJCIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0xheW91dC5qcyIsInNvdXJjZXNDb250ZW50IjpbImltcG9ydCBSZWFjdCwgeyBDb21wb25lbnQgfSBmcm9tICdyZWFjdCc7XG5pbXBvcnQgSGVhZCBmcm9tICduZXh0L2hlYWQnXG5pbXBvcnQgJy4uL3N0eWxlcy9zdHlsZXMuc2FzcydcbmltcG9ydCBMaW5rIGZyb20gJ25leHQvbGluaydcbmltcG9ydCBmYiBmcm9tICcuLi9maXJlYmFzZS5qcydcbmltcG9ydCBMb2dpbk1vZGFsIGZyb20gJy4vTG9naW5Nb2RhbCdcbmltcG9ydCBSb3V0ZXIgZnJvbSAnbmV4dC9yb3V0ZXInXG5cbmNsYXNzIExheW91dCBleHRlbmRzIENvbXBvbmVudCB7XG5cbiAgY29uc3RydWN0b3IoKSB7XG4gICAgc3VwZXIoKTtcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgdXNlcjogbnVsbCxcbiAgICAgIG1vZGFsQWN0aXZlOiBmYWxzZVxuICAgIH1cbiAgICB0aGlzLnNpZ25PdXQgPSB0aGlzLnNpZ25PdXQuYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZU1vZGFsID0gdGhpcy5oYW5kbGVNb2RhbC5iaW5kKHRoaXMpO1xuICAgIHRoaXMubG9naW5DbGlja2VkID0gdGhpcy5sb2dpbkNsaWNrZWQuYmluZCh0aGlzKTtcbiAgfVxuXG4gIGNvbXBvbmVudERpZE1vdW50KCkge1xuICAgIGNvbnN0IHVzZXIgPSBmYi5hdXRoKCkuY3VycmVudFVzZXI7XG4gICAgaWYgKHVzZXIpIHtcbiAgICAgIHRoaXMuc2V0U3RhdGUoeyB1c2VyIH0pO1xuICAgIH1cbiAgfVxuXG4gIHNpZ25PdXQoKSB7XG4gICAgZmIuYXV0aCgpLnNpZ25PdXQoKS50aGVuKGZ1bmN0aW9uKCkge1xuICAgICAgdGhpcy5zZXRTdGF0ZSh7IHVzZXI6IG51bGwgfSlcbiAgICAgIFJvdXRlci5wdXNoKCcvJylcbiAgICB9LmJpbmQodGhpcykpLmNhdGNoKGZ1bmN0aW9uKGVycm9yKSB7XG4gICAgICAvLyBBbiBlcnJvciBoYXBwZW5lZC5cbiAgICB9KTtcbiAgfVxuXG4gIGxvZ2luQ2xpY2tlZCgpIHtcbiAgICB0aGlzLnNldFN0YXRlKHsgbW9kYWxBY3RpdmU6IHRydWV9KVxuICB9XG5cbiAgaGFuZGxlTW9kYWwoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7bW9kYWxBY3RpdmU6IGZhbHNlfSlcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBsZXQgbW9kYWw7XG4gICAgbGV0IHVzZXJCdXR0b25zO1xuICAgIGlmKHRoaXMuc3RhdGUudXNlcikge1xuICAgICAgdXNlckJ1dHRvbnMgPSAoXG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiYnV0dG9uc1wiPlxuICAgICAgICAgIDxMaW5rIGhyZWY9XCIvY29uc29sZVwiPlxuICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwiYnV0dG9uIGlzLWluZm8gaXMtb3V0bGluZWRcIj5cbiAgICAgICAgICAgICAgR28gVG8gQ29uc29sZVxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgIDwvTGluaz5cbiAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ1dHRvbiBpcy1pbmZvIGlzLW91dGxpbmVkXCIgb25DbGljaz17dGhpcy5zaWduT3V0fT5cbiAgICAgICAgICAgIFNpZ24gT3V0XG4gICAgICAgICAgPC9idXR0b24+XG4gICAgICAgIDwvZGl2PlxuICAgICAgKVxuICAgIH0gZWxzZSB7XG4gICAgICB1c2VyQnV0dG9ucyA9IChcbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJidXR0b25zXCI+XG4gICAgICAgICAgPExpbmsgaHJlZj1cIi9zaWdudXBcIj5cbiAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cImJ1dHRvbiBpcy1pbmZvIGlzLW91dGxpbmVkXCI+XG4gICAgICAgICAgICAgIFNpZ24gdXBcbiAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICA8L0xpbms+XG4gICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b24gaXMtaW5mbyBpcy1vdXRsaW5lZFwiIG9uQ2xpY2s9e3RoaXMubG9naW5DbGlja2VkfT5cbiAgICAgICAgICAgIExvZyBpblxuICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICA8L2Rpdj5cbiAgICAgIClcbiAgICB9XG5cbiAgICBpZih0aGlzLnN0YXRlLm1vZGFsQWN0aXZlKSB7XG4gICAgICBtb2RhbCA9IDxMb2dpbk1vZGFsIGhhbmRsZXI9e3RoaXMuaGFuZGxlTW9kYWx9Lz5cbiAgICB9IGVsc2Uge1xuICAgICAgbW9kYWwgPSAnJztcbiAgICB9XG5cbiAgICByZXR1cm4oXG4gICAgICA8ZGl2PlxuICAgICAgICA8SGVhZD5cbiAgICAgICAgICA8bWV0YSBjaGFyc2V0PVwidXRmLThcIiAvPlxuICAgICAgICAgIDxtZXRhIG5hbWU9XCJ2aWV3cG9ydFwiIGNvbnRlbnQ9XCJ3aWR0aD1kZXZpY2Utd2lkdGgsIGluaXRpYWwtc2NhbGU9MVwiIC8+XG4gICAgICAgICAgPHRpdGxlPmdyaXAuaW88L3RpdGxlPlxuICAgICAgICAgIDxzY3JpcHQgZGVmZXIgc3JjPVwiaHR0cHM6Ly91c2UuZm9udGF3ZXNvbWUuY29tL3JlbGVhc2VzL3Y1LjMuMS9qcy9hbGwuanNcIj48L3NjcmlwdD5cbiAgICAgICAgPC9IZWFkPlxuICAgICAgICB7bW9kYWx9XG4gICAgICAgIDxuYXYgY2xhc3NOYW1lPVwibmF2YmFyIGlzLXdoaXRlXCIgcm9sZT1cIm5hdmlnYXRpb25cIiBhcmlhLWxhYmVsPVwibWFpbiBuYXZpZ2F0aW9uXCI+XG4gICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuYXZiYXItYnJhbmRcIj5cbiAgICAgICAgICAgIDxMaW5rIGhyZWY9XCIvXCI+XG4gICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm5hdmJhci1pdGVtXCI+XG4gICAgICAgICAgICAgICAgPGltZyBzcmM9eycuLi9zdGF0aWMvZ3JpcF9sb2dvLnBuZyd9IHdpZHRoPVwiMTI1XCIgaGVpZ2h0PVwiNjBcIi8+XG4gICAgICAgICAgICAgIDwvYT5cbiAgICAgICAgICAgIDwvTGluaz5cblxuICAgICAgICAgICAgPGEgcm9sZT1cImJ1dHRvblwiIGNsYXNzTmFtZT1cIm5hdmJhci1idXJnZXIgYnVyZ2VyXCIgYXJpYS1sYWJlbD1cIm1lbnVcIiBhcmlhLWV4cGFuZGVkPVwiZmFsc2VcIiBkYXRhLXRhcmdldD1cIm5hdmJhckJhc2ljRXhhbXBsZVwiPlxuICAgICAgICAgICAgICA8c3BhbiBhcmlhLWhpZGRlbj1cInRydWVcIj48L3NwYW4+XG4gICAgICAgICAgICAgIDxzcGFuIGFyaWEtaGlkZGVuPVwidHJ1ZVwiPjwvc3Bhbj5cbiAgICAgICAgICAgICAgPHNwYW4gYXJpYS1oaWRkZW49XCJ0cnVlXCI+PC9zcGFuPlxuICAgICAgICAgICAgPC9hPlxuICAgICAgICAgIDwvZGl2PlxuXG4gICAgICAgICAgPGRpdiBpZD1cIm5hdmJhckJhc2ljRXhhbXBsZVwiIGNsYXNzTmFtZT1cIm5hdmJhci1tZW51XCI+XG4gICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5hdmJhci1zdGFydFwiPlxuICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJuYXZiYXItaXRlbVwiPlxuICAgICAgICAgICAgICAgIEhvbWVcbiAgICAgICAgICAgICAgPC9hPlxuXG4gICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm5hdmJhci1pdGVtXCI+XG4gICAgICAgICAgICAgICAgQWJvdXRcbiAgICAgICAgICAgICAgPC9hPlxuXG4gICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibmF2YmFyLWl0ZW0gaGFzLWRyb3Bkb3duIGlzLWhvdmVyYWJsZVwiPlxuICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm5hdmJhci1saW5rXCI+XG4gICAgICAgICAgICAgICAgICBNb3JlXG4gICAgICAgICAgICAgICAgPC9hPlxuXG4gICAgICAgICAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJuYXZiYXItZHJvcGRvd25cIj5cbiAgICAgICAgICAgICAgICAgIDxhIGNsYXNzTmFtZT1cIm5hdmJhci1pdGVtXCI+XG4gICAgICAgICAgICAgICAgICAgIEFib3V0XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJuYXZiYXItaXRlbVwiPlxuICAgICAgICAgICAgICAgICAgICBKb2JzXG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICA8YSBjbGFzc05hbWU9XCJuYXZiYXItaXRlbVwiPlxuICAgICAgICAgICAgICAgICAgICBDb250YWN0XG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgICA8aHIgY2xhc3NOYW1lPVwibmF2YmFyLWRpdmlkZXJcIiAvPlxuICAgICAgICAgICAgICAgICAgPGEgY2xhc3NOYW1lPVwibmF2YmFyLWl0ZW1cIj5cbiAgICAgICAgICAgICAgICAgICAgUmVwb3J0IGFuIGlzc3VlXG4gICAgICAgICAgICAgICAgICA8L2E+XG4gICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibmF2YmFyLWVuZFwiPlxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cIm5hdmJhci1pdGVtXCI+XG4gICAgICAgICAgICAgICAge3VzZXJCdXR0b25zfVxuICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L25hdj5cbiAgICAgICAge3RoaXMucHJvcHMuY2hpbGRyZW59XG4gICAgICAgIDxzdHlsZSBqc3ggZ2xvYmFsPntgXG5cbiAgICAgICAgICBgfVxuICAgICAgICA8L3N0eWxlPlxuICAgICAgPC9kaXY+XG4gICAgKVxuICB9XG59XG5cblxuZXhwb3J0IGRlZmF1bHQgTGF5b3V0O1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/components/Layout.js */",
        __self: this
      }));
    }
  }]);

  return Layout;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Layout);

/***/ }),

/***/ "./components/LoginModal.js":
/*!**********************************!*\
  !*** ./components/LoginModal.js ***!
  \**********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! styled-jsx/style */ "styled-jsx/style");
/* harmony import */ var styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _firebase_js__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../firebase.js */ "./firebase.js");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_3___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_3__);
var _jsxFileName = "/Users/andyyalung/gripio/components/LoginModal.js";


function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }





var LoginModal =
/*#__PURE__*/
function (_Component) {
  _inherits(LoginModal, _Component);

  function LoginModal() {
    var _this;

    _classCallCheck(this, LoginModal);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(LoginModal).call(this));
    _this.state = {
      email: '',
      password: '',
      emailHelpText: '',
      passwordHelpText: ''
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleLogin = _this.handleLogin.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEmailFocus = _this.handleEmailFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handlePasswordFocus = _this.handlePasswordFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(LoginModal, [{
    key: "handleChange",
    value: function handleChange(e) {
      this.setState(_defineProperty({}, e.target.name, e.target.value));
    }
  }, {
    key: "handleLogin",
    value: function handleLogin() {
      var _this2 = this;

      _firebase_js__WEBPACK_IMPORTED_MODULE_2__["default"].auth().signInWithEmailAndPassword(this.state.email, this.state.password).catch(function (error) {
        var errorCode = error.code;
        var errorMessage = error.message;

        if (errorCode == 'auth/invalid-email' || errorCode == 'auth/user-not-found') {
          this.setState({
            emailHelpText: 'Email not found.'
          });
        }

        if (errorCode == 'auth/wrong-password') {
          this.setState({
            passwordHelpText: 'The password is invalid.'
          });
        }
      }.bind(this)).then(function () {
        var user = _firebase_js__WEBPACK_IMPORTED_MODULE_2__["default"].auth().currentUser;

        if (user) {
          _this2.props.handler();

          next_router__WEBPACK_IMPORTED_MODULE_3___default.a.push('/console');
        }
      });
    }
  }, {
    key: "handleEmailFocus",
    value: function handleEmailFocus() {
      this.setState({
        emailHelpText: ''
      });
    }
  }, {
    key: "handlePasswordFocus",
    value: function handlePasswordFocus() {
      this.setState({
        passwordHelpText: ''
      });
    }
  }, {
    key: "render",
    value: function render() {
      var emailStyle = ["input", "is-rounded"];
      var passwordStyle = ["input", "is-rounded"];

      if (this.state.emailHelpText != '') {
        emailStyle.push('is-danger');
      } else {
        emailStyle = ["input", "is-rounded"];
      }

      if (this.state.passwordHelpText != '') {
        passwordStyle.push('is-danger');
      } else {
        passwordStyle = ["input", "is-rounded"];
      }

      return react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "modal is-active",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 68
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "modal-background",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 69
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "modal-card",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 70
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("header", {
        className: "jsx-881260932" + " " + "modal-card-head",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 71
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-881260932" + " " + "modal-card-title has-text-info",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 72
        },
        __self: this
      }, "Login"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        "aria-label": "close",
        onClick: this.props.handler,
        className: "jsx-881260932" + " " + "delete",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 73
        },
        __self: this
      })), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("section", {
        className: "jsx-881260932" + " " + "modal-card-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 75
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 76
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-881260932" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 77
        },
        __self: this
      }, "Email"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 78
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "email",
        type: "email",
        placeholder: "e.g. alexsmith@gmail.com",
        value: this.state.email,
        onChange: this.handleChange,
        onFocus: this.handleEmailFocus,
        className: "jsx-881260932" + " " + (emailStyle.join(' ') || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 79
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "jsx-881260932" + " " + "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 88
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
        className: "jsx-881260932" + " " + "fas fa-envelope",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 89
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-881260932" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 92
        },
        __self: this
      }, this.state.emailHelpText)), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 95
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("label", {
        className: "jsx-881260932" + " " + "label",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 96
        },
        __self: this
      }, "Password"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("div", {
        className: "jsx-881260932" + " " + "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 97
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("input", {
        name: "password",
        type: "password",
        value: this.state.password,
        onChange: this.handleChange,
        onFocus: this.handlePasswordFocus,
        className: "jsx-881260932" + " " + (passwordStyle.join(' ') || ""),
        __source: {
          fileName: _jsxFileName,
          lineNumber: 98
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("span", {
        className: "jsx-881260932" + " " + "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 106
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("i", {
        className: "jsx-881260932" + " " + "fas fa-lock",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 107
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("p", {
        className: "jsx-881260932" + " " + "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, this.state.passwordHelpText))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("footer", {
        className: "jsx-881260932" + " " + "modal-card-foot",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: this.handleLogin,
        className: "jsx-881260932" + " " + "button is-info is-outlined",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, "Login"), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement("button", {
        onClick: this.props.handler,
        className: "jsx-881260932" + " " + "button is-danger is-outlined",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 117
        },
        __self: this
      }, "Cancel"))), react__WEBPACK_IMPORTED_MODULE_1___default.a.createElement(styled_jsx_style__WEBPACK_IMPORTED_MODULE_0___default.a, {
        styleId: "881260932",
        css: ".modal-card.jsx-881260932{width:500px;}\n/*# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIi9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0xvZ2luTW9kYWwuanMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBeUhvQixBQUcyQixZQUNkIiwiZmlsZSI6Ii9Vc2Vycy9hbmR5eWFsdW5nL2dyaXBpby9jb21wb25lbnRzL0xvZ2luTW9kYWwuanMiLCJzb3VyY2VzQ29udGVudCI6WyJpbXBvcnQgUmVhY3QsIHsgQ29tcG9uZW50IH0gZnJvbSAncmVhY3QnO1xuaW1wb3J0IGZiIGZyb20gJy4uL2ZpcmViYXNlLmpzJ1xuaW1wb3J0IFJvdXRlciBmcm9tICduZXh0L3JvdXRlcidcblxuY2xhc3MgTG9naW5Nb2RhbCBleHRlbmRzIENvbXBvbmVudCB7XG4gIGNvbnN0cnVjdG9yKCkge1xuICAgIHN1cGVyKClcbiAgICB0aGlzLnN0YXRlID0ge1xuICAgICAgZW1haWw6ICcnLFxuICAgICAgcGFzc3dvcmQ6ICcnLFxuICAgICAgZW1haWxIZWxwVGV4dDogJycsXG4gICAgICBwYXNzd29yZEhlbHBUZXh0OiAnJ1xuICAgIH1cbiAgICB0aGlzLmhhbmRsZUNoYW5nZSA9IHRoaXMuaGFuZGxlQ2hhbmdlLmJpbmQodGhpcyk7XG4gICAgdGhpcy5oYW5kbGVMb2dpbiA9IHRoaXMuaGFuZGxlTG9naW4uYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZUVtYWlsRm9jdXMgPSB0aGlzLmhhbmRsZUVtYWlsRm9jdXMuYmluZCh0aGlzKTtcbiAgICB0aGlzLmhhbmRsZVBhc3N3b3JkRm9jdXMgPSB0aGlzLmhhbmRsZVBhc3N3b3JkRm9jdXMuYmluZCh0aGlzKTtcbiAgfVxuXG4gIGhhbmRsZUNoYW5nZShlKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IFtlLnRhcmdldC5uYW1lXTogZS50YXJnZXQudmFsdWUgfSk7XG4gIH1cblxuICBoYW5kbGVMb2dpbigpIHtcbiAgICBmYi5hdXRoKCkuc2lnbkluV2l0aEVtYWlsQW5kUGFzc3dvcmQodGhpcy5zdGF0ZS5lbWFpbCwgdGhpcy5zdGF0ZS5wYXNzd29yZClcbiAgICAuY2F0Y2goZnVuY3Rpb24oZXJyb3IpIHtcbiAgICAgIGNvbnN0IGVycm9yQ29kZSA9IGVycm9yLmNvZGU7XG4gICAgICBjb25zdCBlcnJvck1lc3NhZ2UgPSBlcnJvci5tZXNzYWdlO1xuICAgICAgaWYoZXJyb3JDb2RlID09ICdhdXRoL2ludmFsaWQtZW1haWwnIHx8IGVycm9yQ29kZSA9PSAnYXV0aC91c2VyLW5vdC1mb3VuZCcpe1xuICAgICAgICB0aGlzLnNldFN0YXRlKHsgZW1haWxIZWxwVGV4dDogJ0VtYWlsIG5vdCBmb3VuZC4nIH0pXG4gICAgICB9XG4gICAgICBpZihlcnJvckNvZGUgPT0gJ2F1dGgvd3JvbmctcGFzc3dvcmQnKSB7XG4gICAgICAgIHRoaXMuc2V0U3RhdGUoeyBwYXNzd29yZEhlbHBUZXh0OiAnVGhlIHBhc3N3b3JkIGlzIGludmFsaWQuJyB9KVxuICAgICAgfVxuICAgIH0uYmluZCh0aGlzKSkudGhlbigoKSA9PiB7XG4gICAgICBjb25zdCB1c2VyID0gZmIuYXV0aCgpLmN1cnJlbnRVc2VyO1xuICAgICAgaWYgKHVzZXIpIHtcbiAgICAgICAgdGhpcy5wcm9wcy5oYW5kbGVyKCk7XG4gICAgICAgIFJvdXRlci5wdXNoKCcvY29uc29sZScpXG4gICAgICB9XG4gICAgfSlcbiAgfVxuXG4gIGhhbmRsZUVtYWlsRm9jdXMoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IGVtYWlsSGVscFRleHQ6ICcnfSlcbiAgfVxuXG4gIGhhbmRsZVBhc3N3b3JkRm9jdXMoKSB7XG4gICAgdGhpcy5zZXRTdGF0ZSh7IHBhc3N3b3JkSGVscFRleHQ6ICcnfSlcbiAgfVxuXG4gIHJlbmRlcigpIHtcbiAgICBsZXQgZW1haWxTdHlsZSA9IFtcImlucHV0XCIsIFwiaXMtcm91bmRlZFwiXVxuICAgIGxldCBwYXNzd29yZFN0eWxlID0gW1wiaW5wdXRcIiwgXCJpcy1yb3VuZGVkXCJdXG5cbiAgICBpZih0aGlzLnN0YXRlLmVtYWlsSGVscFRleHQgIT0gJycpe1xuICAgICAgZW1haWxTdHlsZS5wdXNoKCdpcy1kYW5nZXInKVxuICAgIH0gZWxzZSB7XG4gICAgICBlbWFpbFN0eWxlID0gW1wiaW5wdXRcIiwgXCJpcy1yb3VuZGVkXCJdXG4gICAgfVxuICAgIGlmKHRoaXMuc3RhdGUucGFzc3dvcmRIZWxwVGV4dCAhPSAnJykge1xuICAgICAgcGFzc3dvcmRTdHlsZS5wdXNoKCdpcy1kYW5nZXInKVxuICAgIH0gZWxzZSB7XG4gICAgICBwYXNzd29yZFN0eWxlID0gW1wiaW5wdXRcIiwgXCJpcy1yb3VuZGVkXCJdXG4gICAgfVxuXG4gICAgcmV0dXJuKFxuICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbCBpcy1hY3RpdmVcIj5cbiAgICAgICAgPGRpdiBjbGFzc05hbWU9XCJtb2RhbC1iYWNrZ3JvdW5kXCI+PC9kaXY+XG4gICAgICAgIDxkaXYgY2xhc3NOYW1lPVwibW9kYWwtY2FyZFwiPlxuICAgICAgICAgIDxoZWFkZXIgY2xhc3NOYW1lPVwibW9kYWwtY2FyZC1oZWFkXCI+XG4gICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJtb2RhbC1jYXJkLXRpdGxlIGhhcy10ZXh0LWluZm9cIj5Mb2dpbjwvcD5cbiAgICAgICAgICAgIDxidXR0b24gY2xhc3NOYW1lPVwiZGVsZXRlXCIgYXJpYS1sYWJlbD1cImNsb3NlXCIgb25DbGljaz17dGhpcy5wcm9wcy5oYW5kbGVyfT48L2J1dHRvbj5cbiAgICAgICAgICA8L2hlYWRlcj5cbiAgICAgICAgICA8c2VjdGlvbiBjbGFzc05hbWU9XCJtb2RhbC1jYXJkLWJvZHlcIj5cbiAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiZmllbGRcIj5cbiAgICAgICAgICAgICAgIDxsYWJlbCBjbGFzc05hbWU9XCJsYWJlbFwiPkVtYWlsPC9sYWJlbD5cbiAgICAgICAgICAgICAgIDxkaXYgY2xhc3NOYW1lPVwiY29udHJvbCBoYXMtaWNvbnMtbGVmdFwiPlxuICAgICAgICAgICAgICAgICA8aW5wdXRcbiAgICAgICAgICAgICAgICAgICBuYW1lPVwiZW1haWxcIlxuICAgICAgICAgICAgICAgICAgIGNsYXNzTmFtZT17ZW1haWxTdHlsZS5qb2luKCcgJyl9XG4gICAgICAgICAgICAgICAgICAgdHlwZT1cImVtYWlsXCJcbiAgICAgICAgICAgICAgICAgICBwbGFjZWhvbGRlcj1cImUuZy4gYWxleHNtaXRoQGdtYWlsLmNvbVwiXG4gICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUuZW1haWx9XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMuaGFuZGxlRW1haWxGb2N1c31cbiAgICAgICAgICAgICAgICAgLz5cbiAgICAgICAgICAgICAgICAgPHNwYW4gY2xhc3NOYW1lPVwiaWNvbiBpcy1zbWFsbCBpcy1sZWZ0XCI+XG4gICAgICAgICAgICAgICAgICAgPGkgY2xhc3NOYW1lPVwiZmFzIGZhLWVudmVsb3BlXCI+PC9pPlxuICAgICAgICAgICAgICAgICA8L3NwYW4+XG4gICAgICAgICAgICAgICA8L2Rpdj5cbiAgICAgICAgICAgICAgIDxwIGNsYXNzTmFtZT1cImhlbHAgaXMtZGFuZ2VyXCI+e3RoaXMuc3RhdGUuZW1haWxIZWxwVGV4dH08L3A+XG4gICAgICAgICAgICAgPC9kaXY+XG5cbiAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImZpZWxkXCI+XG4gICAgICAgICAgICAgICA8bGFiZWwgY2xhc3NOYW1lPVwibGFiZWxcIj5QYXNzd29yZDwvbGFiZWw+XG4gICAgICAgICAgICAgICA8ZGl2IGNsYXNzTmFtZT1cImNvbnRyb2wgaGFzLWljb25zLWxlZnRcIj5cbiAgICAgICAgICAgICAgICAgPGlucHV0XG4gICAgICAgICAgICAgICAgICAgbmFtZT1cInBhc3N3b3JkXCJcbiAgICAgICAgICAgICAgICAgICBjbGFzc05hbWU9e3Bhc3N3b3JkU3R5bGUuam9pbignICcpfVxuICAgICAgICAgICAgICAgICAgIHR5cGU9XCJwYXNzd29yZFwiXG4gICAgICAgICAgICAgICAgICAgdmFsdWU9e3RoaXMuc3RhdGUucGFzc3dvcmR9XG4gICAgICAgICAgICAgICAgICAgb25DaGFuZ2U9e3RoaXMuaGFuZGxlQ2hhbmdlfVxuICAgICAgICAgICAgICAgICAgIG9uRm9jdXM9e3RoaXMuaGFuZGxlUGFzc3dvcmRGb2N1c31cbiAgICAgICAgICAgICAgICAvPlxuICAgICAgICAgICAgICAgICA8c3BhbiBjbGFzc05hbWU9XCJpY29uIGlzLXNtYWxsIGlzLWxlZnRcIj5cbiAgICAgICAgICAgICAgICAgICA8aSBjbGFzc05hbWU9XCJmYXMgZmEtbG9ja1wiPjwvaT5cbiAgICAgICAgICAgICAgICAgPC9zcGFuPlxuICAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgICA8cCBjbGFzc05hbWU9XCJoZWxwIGlzLWRhbmdlclwiPnt0aGlzLnN0YXRlLnBhc3N3b3JkSGVscFRleHR9PC9wPlxuICAgICAgICAgICAgIDwvZGl2PlxuICAgICAgICAgIDwvc2VjdGlvbj5cbiAgICAgICAgICA8Zm9vdGVyIGNsYXNzTmFtZT1cIm1vZGFsLWNhcmQtZm9vdFwiPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzc05hbWU9XCJidXR0b24gaXMtaW5mbyBpcy1vdXRsaW5lZFwiIG9uQ2xpY2s9e3RoaXMuaGFuZGxlTG9naW59PlxuICAgICAgICAgICAgICBMb2dpblxuICAgICAgICAgICAgPC9idXR0b24+XG4gICAgICAgICAgICA8YnV0dG9uIGNsYXNzTmFtZT1cImJ1dHRvbiBpcy1kYW5nZXIgaXMtb3V0bGluZWRcIiBvbkNsaWNrPXt0aGlzLnByb3BzLmhhbmRsZXJ9PlxuICAgICAgICAgICAgICBDYW5jZWxcbiAgICAgICAgICAgIDwvYnV0dG9uPlxuICAgICAgICAgIDwvZm9vdGVyPlxuICAgICAgICA8L2Rpdj5cbiAgICAgICAgPHN0eWxlIGpzeD57YFxuICAgICAgICAgICAgLm1vZGFsLWNhcmQge1xuICAgICAgICAgICAgICB3aWR0aDogNTAwcHg7XG4gICAgICAgICAgICB9XG4gICAgICAgICAgYH1cbiAgICAgICAgPC9zdHlsZT5cbiAgICAgIDwvZGl2PlxuICAgIClcbiAgfVxufVxuXG5leHBvcnQgZGVmYXVsdCBMb2dpbk1vZGFsO1xuIl19 */\n/*@ sourceURL=/Users/andyyalung/gripio/components/LoginModal.js */",
        __self: this
      }));
    }
  }]);

  return LoginModal;
}(react__WEBPACK_IMPORTED_MODULE_1__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (LoginModal);

/***/ }),

/***/ "./firebase.js":
/*!*********************!*\
  !*** ./firebase.js ***!
  \*********************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! firebase */ "firebase");
/* harmony import */ var firebase__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(firebase__WEBPACK_IMPORTED_MODULE_0__);

var config = {
  apiKey: "AIzaSyBnPR-8wZTA-UhJnqlotJZbZh96L7r1Sok",
  authDomain: "gripio-374d7.firebaseapp.com",
  databaseURL: "https://gripio-374d7.firebaseio.com",
  projectId: "gripio-374d7",
  storageBucket: "gripio-374d7.appspot.com",
  messagingSenderId: "940281243850"
};
/* harmony default export */ __webpack_exports__["default"] = (!firebase__WEBPACK_IMPORTED_MODULE_0___default.a.apps.length ? firebase__WEBPACK_IMPORTED_MODULE_0___default.a.initializeApp(config) : firebase__WEBPACK_IMPORTED_MODULE_0___default.a.app());

/***/ }),

/***/ "./pages/signup.js":
/*!*************************!*\
  !*** ./pages/signup.js ***!
  \*************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react */ "react");
/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! next/router */ "next/router");
/* harmony import */ var next_router__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(next_router__WEBPACK_IMPORTED_MODULE_1__);
/* harmony import */ var _components_Layout__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../components/Layout */ "./components/Layout.js");
/* harmony import */ var _firebase_js__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../firebase.js */ "./firebase.js");
var _jsxFileName = "/Users/andyyalung/gripio/pages/signup.js";

function _typeof(obj) { if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }






var Signup =
/*#__PURE__*/
function (_Component) {
  _inherits(Signup, _Component);

  function Signup() {
    var _this;

    _classCallCheck(this, Signup);

    _this = _possibleConstructorReturn(this, _getPrototypeOf(Signup).call(this));
    _this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      validEmail: true,
      validPassword: true,
      validConfirm: true,
      emailHelpText: '',
      passwordHelpText: '',
      confirmHelpText: ''
    };
    _this.handleChange = _this.handleChange.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleSubmit = _this.handleSubmit.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleEmailFocus = _this.handleEmailFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handlePasswordFocus = _this.handlePasswordFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    _this.handleConfirmFocus = _this.handleConfirmFocus.bind(_assertThisInitialized(_assertThisInitialized(_this)));
    return _this;
  }

  _createClass(Signup, [{
    key: "handleEmailFocus",
    value: function handleEmailFocus() {
      this.setState({
        validEmail: true
      });
      this.setState({
        emailHelpText: ''
      });
    }
  }, {
    key: "handlePasswordFocus",
    value: function handlePasswordFocus() {
      this.setState({
        validPassword: true
      });
      this.setState({
        passwordHelpText: ''
      });
    }
  }, {
    key: "handleConfirmFocus",
    value: function handleConfirmFocus() {
      this.setState({
        validConfirm: true
      });
      this.setState({
        confirmHelpText: ''
      });
    }
  }, {
    key: "handleChange",
    value: function handleChange(e) {
      this.setState(_defineProperty({}, e.target.name, e.target.value));
    }
  }, {
    key: "handleSubmit",
    value: function handleSubmit() {
      var validInputs = true;

      if (this.state.email == '') {
        validInputs = false;
        this.setState({
          validEmail: false
        });
        this.setState({
          emailHelpText: 'Email required.'
        });
      }

      if (this.state.password == '') {
        validInputs = false;
        this.setState({
          validPassword: false
        });
        this.setState({
          passwordHelpText: 'Password required.'
        });
      }

      if (this.state.password != this.state.confirmPassword) {
        validInputs = false;
        this.setState({
          validConfirm: false
        });
        this.setState({
          confirmHelpText: 'Passwords must match.'
        });
      }

      if (validInputs) {
        _firebase_js__WEBPACK_IMPORTED_MODULE_3__["default"].auth().createUserWithEmailAndPassword(this.state.email, this.state.password).catch(function (error) {
          var errorCode = error.code;
          var errorMessage = error.message;
          console.log(errorMessage);

          if (errorCode == 'auth/email-already-in-use') {
            this.setState({
              validEmail: false
            });
            this.setState({
              emailHelpText: errorMessage
            });
          } else if (errorCode == 'auth/invalid-email') {
            this.setState({
              validEmail: false
            });
            this.setState({
              emailHelpText: errorMessage
            });
          } else if (errorCode == 'auth/weak-password') {
            this.setState({
              validPassword: false
            });
            this.setState({
              passwordHelpText: errorMessage
            });
          }
        }.bind(this)).then(function () {
          var user = _firebase_js__WEBPACK_IMPORTED_MODULE_3__["default"].auth().currentUser;

          if (user) {
            next_router__WEBPACK_IMPORTED_MODULE_1___default.a.push('/');
          }
        });
      }
    }
  }, {
    key: "render",
    value: function render() {
      var emailStyle = ["input", "is-rounded"];
      var passwordStyle = ["input", "is-rounded"];
      var confirmStyle = ["input", "is-rounded"];

      if (!this.state.validEmail) {
        emailStyle.push('is-danger');
      } else {
        emailStyle = ["input", "is-rounded"];
      }

      if (!this.state.validPassword) {
        passwordStyle.push('is-danger');
      } else {
        passwordStyle = ["input", "is-rounded"];
      }

      if (!this.state.validConfirm) {
        confirmStyle.push('is-danger');
      } else {
        confirmStyle = ["input", "is-rounded"];
      }

      return react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement(_components_Layout__WEBPACK_IMPORTED_MODULE_2__["default"], {
        __source: {
          fileName: _jsxFileName,
          lineNumber: 109
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("section", {
        className: "hero is-fullheight-with-navbar is-info",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 110
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "hero-body",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 111
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "container",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 112
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "columns is-vcentered",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 113
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "column has-text-centered",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 114
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h1", {
        className: "title",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 115
        },
        __self: this
      }, "Sign up with us today!"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("h2", {
        className: "subtitle",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 118
        },
        __self: this
      }, "grip.io allows you to effortlessly communicate with your members.")), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "column has-text-centered",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 122
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 124
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        className: "label has-text-white",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 125
        },
        __self: this
      }, "Email"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 126
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        name: "email",
        className: emailStyle.join(' '),
        value: this.state.email,
        onChange: this.handleChange,
        type: "email",
        placeholder: "e.g. alexsmith@gmail.com",
        onFocus: this.handleEmailFocus,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 127
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 135
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fas fa-envelope",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 136
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 139
        },
        __self: this
      }, this.state.emailHelpText)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 142
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        className: "label has-text-white",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 143
        },
        __self: this
      }, "Password"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 144
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        name: "password",
        className: passwordStyle.join(' '),
        type: "password",
        value: this.state.password,
        onChange: this.handleChange,
        onFocus: this.handlePasswordFocus,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 145
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 152
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fas fa-lock",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 153
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 156
        },
        __self: this
      }, this.state.passwordHelpText)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "field",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 159
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("label", {
        className: "label has-text-white",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 160
        },
        __self: this
      }, "Confirm Password"), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("div", {
        className: "control has-icons-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 161
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("input", {
        name: "confirmPassword",
        className: confirmStyle.join(' '),
        type: "password",
        value: this.state.confirmPassword,
        onChange: this.handleChange,
        onFocus: this.handleConfirmFocus,
        __source: {
          fileName: _jsxFileName,
          lineNumber: 162
        },
        __self: this
      }), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("span", {
        className: "icon is-small is-left",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 169
        },
        __self: this
      }, react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("i", {
        className: "fas fa-lock",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 170
        },
        __self: this
      }))), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("p", {
        className: "help is-danger",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 173
        },
        __self: this
      }, this.state.confirmHelpText)), react__WEBPACK_IMPORTED_MODULE_0___default.a.createElement("button", {
        onClick: this.handleSubmit,
        className: "button is-primary is-inverted is-rounded is-large",
        __source: {
          fileName: _jsxFileName,
          lineNumber: 176
        },
        __self: this
      }, "Sign Up")))))));
    }
  }]);

  return Signup;
}(react__WEBPACK_IMPORTED_MODULE_0__["Component"]);

/* harmony default export */ __webpack_exports__["default"] = (Signup);

/***/ }),

/***/ "./styles/styles.sass":
/*!****************************!*\
  !*** ./styles/styles.sass ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {



/***/ }),

/***/ 4:
/*!*******************************!*\
  !*** multi ./pages/signup.js ***!
  \*******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! ./pages/signup.js */"./pages/signup.js");


/***/ }),

/***/ "firebase":
/*!***************************!*\
  !*** external "firebase" ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("firebase");

/***/ }),

/***/ "next/head":
/*!****************************!*\
  !*** external "next/head" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/head");

/***/ }),

/***/ "next/link":
/*!****************************!*\
  !*** external "next/link" ***!
  \****************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/link");

/***/ }),

/***/ "next/router":
/*!******************************!*\
  !*** external "next/router" ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("next/router");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("react");

/***/ }),

/***/ "styled-jsx/style":
/*!***********************************!*\
  !*** external "styled-jsx/style" ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = require("styled-jsx/style");

/***/ })

/******/ });
//# sourceMappingURL=signup.js.map